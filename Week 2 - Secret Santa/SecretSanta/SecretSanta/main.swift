//
//  main.swift
//  SecretSanta
//
//  Created by Curt Clifton on 9/19/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

/*
Input:

Your script will be fed a list of names on STDIN. An example might be:

Luke Skywalker <luke@theforce.net>
Leia Skywalker <leia@therebellion.org>
Toula Portokalos <toula@manhunter.org>
Gus Portokalos <gus@weareallfruit.net>
Bruce Wayne <bruce@imbatman.com>
Virgil Brigman <virgil@rigworkersunion.org>
Lindsey Brigman <lindsey@iseealiens.net>

Note: If you cannot tell, I made those addresses up and you'll need to replace them with something meaningful. Please don't pester those people, should they happen to be real.

The format for these names is:

FIRST_NAME space FAMILY_NAME space <EMAIL_ADDRESS> newline

We'll keep things simple and say that people only have two names, so you don't have to worry about tricky names like Gray II.

Output:

Your script should then choose a Secret Santa for every name in the list. Obviously, a person cannot be their own Secret Santa. In addition, my friends no longer allow people in the same family to be Santas for each other and your script should take this into account.

GIVER_FIRST space GIVER_FAMILY space GIVER_EMAIL space => space RECIPIENT_FIRST space RECIPIENT_FAMILY space RECIPIENT_EMAIL
*/

//MARK: Helper Functions

func printUsage() {
    print("Usage:")
    print("    SecretSanta <filename> — to read input from filename")
    print("    SecretSanta — to read input from stdin")
}

func printResults(results: [Player: Player]) {
    for (santa, recipient) in results {
        print("\(santa) => \(recipient)")
    }
}


//MARK: Main
func main(arguments: [String]) -> Int32 {
//    let arguments = Process.arguments
    guard arguments.count == 1 || arguments.count == 2 else {
        printUsage()
        return -1
    }
    
    let inputData: NSData
    if arguments.count == 1 {
        let standardInFileHandle = NSFileHandle.fileHandleWithStandardInput()
        inputData = standardInFileHandle.readDataToEndOfFile()
        standardInFileHandle.closeFile()
    } else {
        let fileName = arguments[1]
        do {
            inputData = try NSData(contentsOfFile: fileName, options: [])
        } catch {
            print("Unable to read data from file “\(fileName)”")
            print("Error: \(error)")
            return -1
        }
    }
    
    let inputString = NSString(data: inputData, encoding: NSUTF8StringEncoding)
    var inputLines: [String] = []
    inputString?.enumerateLinesUsingBlock({ (line, _) -> Void in
        inputLines.append(line)
    })
    
    do {
        let players = try inputLines.map { try Player.parse($0) }
        let resultsOfDraw = try drawNames(players)
//        let resultsOfDraw = try drawNamesBrutally(players)
        printResults(resultsOfDraw)
    } catch {
        print("error: “\(error)”")
    }
    return 0
}

exit(main(Process.arguments))

