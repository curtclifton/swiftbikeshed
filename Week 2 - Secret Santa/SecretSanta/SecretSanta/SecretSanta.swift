//
//  SecretSanta.swift
//  SecretSanta
//
//  Created by Curt Clifton on 9/19/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

enum SecretSantaError: ErrorType {
    case ParseError(message: String)
    case Unsolveable
}

struct Player {
    let firstName: String
    let lastName: String
    let email: String
    
    // FIRST_NAME space FAMILY_NAME space <EMAIL_ADDRESS> newline
    static func parse(line: String) throws -> Player {
        let substrings = line.characters.split(" ").map { String($0) }

        let hasEmptyString = substrings.reduce(false) { $0 || $1.isEmpty }
        guard !hasEmptyString && substrings.count == 3 else {
            throw SecretSantaError.ParseError(message: "Expected 3 space-delimited values: “\(line)”")
        }
        
        let firstName = substrings[0]
        let lastName = substrings[1]
        let wrappedEmail = substrings[2]
        
        guard wrappedEmail.hasPrefix("<") && wrappedEmail.hasSuffix(">") else {
            throw SecretSantaError.ParseError(message: "Expected email to be wrapped in “<” and “>”: “\(wrappedEmail)”")
        }
        
        let email = String(wrappedEmail.characters.dropFirst().dropLast())
        
        return Player(firstName: firstName, lastName: lastName, email: email)
    }
}

extension Player: CustomStringConvertible {
    var description: String {
        return "\(self.firstName) \(self.lastName) \(self.email)"
    }
}

extension Player: Equatable {
}

func ==(player1: Player, player2: Player) -> Bool {
    return player1.firstName == player2.firstName && player1.lastName == player2.lastName && player1.email == player2.email
}

extension Player: Hashable {
    var hashValue: Int {
        return (firstName.hashValue << 13) ^ (lastName.hashValue << 7) ^ (email.hashValue)
    }
}

extension CollectionType {
    func categorize<BucketKey: Hashable>(categorizer: Generator.Element -> BucketKey) -> [BucketKey: [Generator.Element]] {
        var result: [BucketKey: [Generator.Element]] = [:]
        for element in self {
            let category = categorizer(element)
            var bucket = result[category] ?? []
            bucket.append(element)
            result[category] = bucket
        }
        return result
    }
}

/// Returns a random number in the range lowerBound..<upperBound
func randRangeWithLowerBound(lowerBound: Int, upperBound: Int) -> Int {
    assert(lowerBound < upperBound)
    let realRandom = Double(arc4random()) / 4294967296.0
    let randInt = Int(floor(Double(upperBound - lowerBound) * realRandom))
    return randInt + lowerBound
}

extension CollectionType where Index.Distance == Int {
    func someValue() -> Generator.Element? {
        if count == 0 {
            return nil
        }
        
        let rangeSize = startIndex.distanceTo(endIndex)
        let offset = randRangeWithLowerBound(0, upperBound: rangeSize)
        let index = startIndex.advancedBy(offset)
        return self[index]
    }
}

func validateResult(result: [Player: Player], players: [Player]) {
    assert(Set(players) == Set(result.keys))
    assert(Set(players) == Set(result.values))
    for (santa, recipient) in result {
        assert(santa.lastName != recipient.lastName)
    }
}

func drawNames(players: [Player]) throws -> [Player: Player] {
    // Partition players by family
    let families = players.categorize({ $0.lastName })

    // Sort families by size
    func sizeOfFamily(familyName: String) -> Int {
        return families[familyName]?.count ?? 0
    }
    let sortedFamilyNames = families.keys.sort { (familyName1, familyName2) in
        sizeOfFamily(familyName1) > sizeOfFamily(familyName2)
    }
    
    // Validate input
    guard players.count > 0 else { return [:] }
    guard sizeOfFamily(sortedFamilyNames.first!) <= players.count / 2 else {
        throw SecretSantaError.Unsolveable
    }
    
    // Conduct the draw, largest family first, repeating until the stars align
    var result: [Player: Player]
    draw: repeat {
        result = [:]
        var alreadyDrawnNames: Set<Player> = Set()
        for familyName in sortedFamilyNames {
            let family = families[familyName]! // key must be present or it wouldn't have made it into the list
            for person in family {
                let drawSet = players.filter { player in
                    return player.lastName != familyName && !alreadyDrawnNames.contains(player)
                }
//                print("For person \(person), the draw set is \(drawSet)")
                guard !drawSet.isEmpty else {
                    // bad luck, throw out the result and try again
//                    print("-------------------------------------------------------------------------")
                    continue draw
                }
                let recipient = drawSet.someValue()! // must be a value based on guard above
                result[person] = recipient
                alreadyDrawnNames.insert(recipient)
            }
        }
    } while result.count < players.count

    validateResult(result, players: players)
    return result
}

/// This implementation recursive derives all legal draws and then chooses one of them at random. It's both brute force and brutally expensive, but it is guaranteed to catch the case where no result is possible. I think the constraint in `drawNames`, that the largest family is no larger than half the draw, is sufficient, but I'm not certain of the fact. Writing a brute force recursion seems easier than doing the proof.
func drawNamesBrutally(players: [Player]) throws -> [Player: Player] {
    func completeDrawWithRemainingSantas(remainingSantas: ArraySlice<Player>, resultSoFar: [Player: Player]) throws -> [[Player: Player]] {
        if remainingSantas.isEmpty {
            return [resultSoFar]
        }
        
        let santa = remainingSantas.first! // must exist because of count check above
        let alreadyDrawn = Set(resultSoFar.values)
        let drawSet = players.filter { possibleRecipient in
            return possibleRecipient.lastName != santa.lastName && !alreadyDrawn.contains(possibleRecipient)
        }
        let newRemainingSantas = remainingSantas.dropFirst()
        
        let recursiveResults = try drawSet.map { (recipient: Player) -> [[Player:Player]] in
            var updatedMap = resultSoFar
            updatedMap[santa] = recipient
            return try completeDrawWithRemainingSantas(newRemainingSantas, resultSoFar: updatedMap)
        }
        let result  = Array(recursiveResults.flatten())
        return result
    }
    
    let possibleDraws = try completeDrawWithRemainingSantas(players[0..<players.count], resultSoFar: [:])
    //    print(possibleDraws)
    
    guard players.count == 0 || possibleDraws.count > 0 else {
        throw SecretSantaError.Unsolveable
    }
    
    if let result = possibleDraws.someValue() {
        validateResult(result, players: players)
        return result
    }
    
    let result: [Player: Player] = [:]
    validateResult(result, players: players)
    return result
}

