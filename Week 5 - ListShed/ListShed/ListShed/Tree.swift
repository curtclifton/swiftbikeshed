//
//  Tree.swift
//  ListShed
//
//  Created by Curt Clifton on 10/20/15.
//  Copyright © 2015 The Omni Group. All rights reserved.
//

import Foundation

protocol Observable {
    typealias Observed
    func addObserverForChange(callback: (Observed) -> ()) -> ObserverID
    func removeObserver(observerID: ObserverID)
}

protocol NodePayload {
    var id: String { get }
    init()
}

protocol TreeModel {
    typealias Payload: NodePayload
    var undoManager: NSUndoManager? {set get}
    var rootNode: TreeNode<Payload> {set get}
}

struct TreeNode<Payload: NodePayload> {
    var payload: Payload
    private var childNodes: [TreeNode<Payload>]
    
    init(payload: Payload) {
        self.init(payload: payload, childNodes: [])
    }
    
    private init(payload: Payload, childNodes: [TreeNode<Payload>]) {
        self.payload = payload
        self.childNodes = childNodes
    }
    
    mutating func insertNewNode() {
        let newPayload = Payload()
        let newNode = TreeNode(payload: newPayload)
        self.childNodes.append(newNode)
    }
    
    mutating func deleteNodeAtIndex(index: Int) {
        self.childNodes.removeAtIndex(index)
    }
}


enum TreeLensError: ErrorType {
    case IndexOutOfBounds
    case IllegalOperationOnRootNode
    case RefocusTargetMissing
}


// We unpack the current level and rebuild items as we move up the tree.
private struct Breadcrumb<Payload: NodePayload> {
    var nodesAtCurrentLevel: [TreeNode<Payload>]
    let nodeIndex: Int
    
    var node: TreeNode<Payload> {
        get {
            return nodesAtCurrentLevel[nodeIndex]
        }
        set {
            nodesAtCurrentLevel[nodeIndex] = newValue
        }
    }
}

struct TreeLens<Payload: NodePayload, Model: protocol<TreeModel, Observable> where Model.Payload == Payload, Model.Observed == Model> {
    private var breadcrumbs: [Breadcrumb<Payload>]
    
    // node is a model object. Don't expose it. Cover any methods we need.
    private var node: TreeNode<Payload> {
        get {
            let result = breadcrumbs.last!.node
            return result
        }
        set {
            breadcrumbs[breadcrumbs.count - 1].node = newValue
            updateModelState() // all mutations via the lens funnel through this setter
        }
    }
    
    private var refocusPath: [String] { // item IDs
        let result = breadcrumbs.dropFirst().map { crumb in
            return crumb.node.payload.id
        }
        return result
    }
    
    private var model: Model
    private var rootNode: TreeNode<Payload> {
        var lens = self
        while lens.breadcrumbs.count > 1 {
            try! lens.moveUp() // loop condition ensures that we can move up
        }
        // lens is focused on root item
        return lens.node
    }
    
    init(model: Model) {
        self.model = model
        let rootBreadcrumb = Breadcrumb(nodesAtCurrentLevel: [model.rootNode], nodeIndex: 0)
        self.breadcrumbs = [rootBreadcrumb]
    }
    
    //MARK: - Internal API
    //MARK: Focusing
    
    mutating func moveUp() throws {
        guard let latestBreadcrumb = breadcrumbs.popLast() where !breadcrumbs.isEmpty else {
            throw TreeLensError.IllegalOperationOnRootNode
        }
        // Update items in the level we're moving to, so the view from this lens is correct.
        let destinationItemchildNodes = latestBreadcrumb.nodesAtCurrentLevel
        node.childNodes = destinationItemchildNodes
    }
    
    mutating func moveDownAtIndex(index: Int) throws {
        guard node.childNodes.isValidIndex(index) else {
            throw TreeLensError.IndexOutOfBounds
        }
        let nodes = node.childNodes
        let newBreadcrumb = Breadcrumb(nodesAtCurrentLevel: nodes, nodeIndex: index)
        breadcrumbs.append(newBreadcrumb)
    }
    
    //MARK: Payload Manipulation
    var payload: Payload {
        get {
            return node.payload
        }
        set {
            node.payload = newValue
        }
    }
    
    var countOfChildren: Int {
        return node.childNodes.count
    }
    
    func payloadForChildAtIndex(index: Int) -> Payload {
        return node.childNodes[index].payload
    }
    
    mutating func insertNewNode() { // CCC, 10/20/2015. hmm, seems like this should really return a new lens focused on the node so it can be manipulated
        node.insertNewNode()
    }
    
    mutating func deleteNodeAtIndex(index: Int) throws {
        node.deleteNodeAtIndex(index)
    }
    
    //MARK: - Private API
    
    private mutating func updateModelState() {
        model.rootNode = rootNode
    }
    
    private mutating func refocusOnPath(path: [String]) throws {
        for payloadID in path {
            // Find the current subitem with an ID matching the path, move the lens down
            let maybeNodeIndex = node.childNodes.indexOf { treeNode in
                return treeNode.payload.id == payloadID
            }
            guard let nodeIndex = maybeNodeIndex else {
                throw TreeLensError.RefocusTargetMissing
            }
            try moveDownAtIndex(nodeIndex)
        }
    }
}

extension TreeLens: Observable {
    func addObserverForChange(callback: (TreeLens<Payload,Model>?) -> ()) -> ObserverID {
        let pathToRestore = refocusPath
        let observerID = model.addObserverForChange { model in
            do {
                var newLens = TreeLens<Payload,Model>(model: model)
                try newLens.refocusOnPath(pathToRestore)
                callback(newLens)
            } catch TreeLensError.RefocusTargetMissing {
                callback(nil)
            } catch {
                abort()
            }
        }
        return observerID
    }
    
    func removeObserver(observerID: ObserverID) {
        model.removeObserver(observerID)
    }
    
}

//MARK: - Utilities

struct ObserverID {
    private static var nextObserverID = 1
    
    private let id: Int
    
    init() {
        self.id = ObserverID.nextObserverID
        ObserverID.nextObserverID++
    }
}

extension ObserverID: Hashable {
    var hashValue: Int {
        return id
    }
}

func ==(lhs: ObserverID, rhs: ObserverID) -> Bool {
    return lhs.id == rhs.id
}

