//
//  Extensions.swift
//  ListShed
//
//  Created by Curt Clifton on 10/17/15.
//  Copyright © 2015 The Omni Group. All rights reserved.
//

import Foundation
import UIKit

@noreturn func finishPorting(message: String? = nil) {
    if let message = message {
        print("Finish porting: \(message)")
    } else {
        print("Finish porting")
    }
    abort()
}

extension Indexable {
    func isValidIndex(index: Index) -> Bool {
        let distanceFromStartToEnd = startIndex.distanceTo(endIndex)
        let distanceToIndex = startIndex.distanceTo(index)
        return distanceToIndex >= 0 && distanceToIndex < distanceFromStartToEnd
    }
}

extension UIView {
    func firstResponderSubview() -> UIView? {
        if isFirstResponder() {
            return self
        }
        for view in subviews {
            if let result = view.firstResponderSubview() {
                return result
            }
        }
        return nil
    }
}