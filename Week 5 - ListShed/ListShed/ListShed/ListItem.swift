//
//  ListItem.swift
//  ListShed
//

import Foundation

//MARK: - Model Types

/*
The model types should not explicitly reference the view model, or anything else really.
*/

// ListModal is the one reference type in the model, allowing us to create a single, shared model underlying the app.
final class ListShedModel: TreeModel, Observable {
    typealias Payload = ListShedPayload
    
    var undoManager: NSUndoManager?
    
    var rootNode: TreeNode<ListShedPayload> = TreeNode(payload: ListShedPayload("Home")) {
        didSet {
            undoManager?.registerUndoWithTarget(self) { model in
                print("undoing/redoing")
                model.rootNode = oldValue
            }
            notifyObservers()
        }
    }
    
    var observers: [ObserverID: (ListShedModel) -> ()] = [:]

    func addObserverForChange(callback: (ListShedModel) -> ()) -> ObserverID {
        let newID = ObserverID()
        observers[newID] = callback
        return newID
    }
    
    func removeObserver(observerID: ObserverID) {
        observers[observerID] = nil
    }
    
    func notifyObservers() {
        for observerCallback in observers.values {
            observerCallback(self)
        }
    }
}

struct ListShedPayload: NodePayload {
    private(set) var id: String
    var name: String
    
    private static var nextNewItemNumber = 1
    
    init() {
        let name = "Item \(ListShedPayload.nextNewItemNumber)"
        ListShedPayload.nextNewItemNumber++
        self.init(name)
    }
    
    private init(_ name: String) {
        let id = CFUUIDCreateString(nil, CFUUIDCreate(nil)) as String
        self.init(name: name, id: id)
    }
    
    private init(name: String, id: String) {
        self.id = id
        self.name = name
    }
}

//MARK: - View Model

/*
The view model explicitly references the model, but does not reference any views or view controllers.
*/

// CCC, 10/21/2015. not sure how best to share code, let's try just covering initially. What we really want is to make these protocols and define an empty extension on TreeLens that makes a TreeLens of the right type conform to the protocol. Swift doesn't support that yet, unfortunately.
struct ListShedDetailViewModel {
    private var lens: TreeLens<ListShedPayload, ListShedModel>
    var name: String {
        get {
            return lens.payload.name
        }
        set {
            lens.payload.name = newValue
        }
    }
}

struct ListShedMasterViewModel {
    private(set) var lens: TreeLens<ListShedPayload, ListShedModel>
    var name: String {
        get {
            return lens.payload.name
        }
        set {
            lens.payload.name = newValue
        }
    }
    
    var detailViewModel: ListShedDetailViewModel {
        // CCC, 10/21/2015. Seems like an odd approach, ListShedMasterViewModel should really be a subtype of ListShedDetailViewModel
        return ListShedDetailViewModel(lens: lens)
    }
    
    mutating func insertNewNode() {
        lens.insertNewNode()
    }

    mutating func deleteNodeAtIndex(index: Int) throws {
        try lens.deleteNodeAtIndex(index)
    }
    
    mutating func moveDownAtIndex(index: Int) throws {
        try lens.moveDownAtIndex(index)
    }
    
    var countOfChildren: Int {
        return lens.countOfChildren
    }
    
    func detailViewModelForChildAtIndex(index: Int) -> ListShedDetailViewModel {
        var detailLens = lens
        try! detailLens.moveDownAtIndex(index) // CCC, 10/21/2015. error handling
        return ListShedDetailViewModel(lens: detailLens)
    }
}

extension ListShedDetailViewModel: Observable {
    func addObserverForChange(callback: (ListShedDetailViewModel?) -> ()) -> ObserverID {
        let observerID = lens.addObserverForChange { maybeLens in
            if let lens = maybeLens {
                callback(ListShedDetailViewModel(lens: lens))
            } else {
                callback(nil)
            }
        }
        return observerID
    }
    
    func removeObserver(observerID: ObserverID) {
        lens.removeObserver(observerID)
    }
}

extension ListShedMasterViewModel: Observable {
    func addObserverForChange(callback: (ListShedMasterViewModel?) -> ()) -> ObserverID {
        let observerID = lens.addObserverForChange { maybeLens in
            if let lens = maybeLens {
                callback(ListShedMasterViewModel(lens: lens))
            } else {
                callback(nil)
            }
        }
        return observerID
    }
    
    func removeObserver(observerID: ObserverID) {
        lens.removeObserver(observerID)
    }
}

