//
//  MasterViewController.swift
//  ListShed
//

import UIKit

class MasterViewController: UITableViewController {
    private var registeredViewModelObserverID: ObserverID?
    
    var viewModel: ListShedMasterViewModel? = nil {
        didSet {
            startObservingViewModelIfNeeded()
            updateForChangeFromViewModel(oldValue, toViewModel: viewModel)
        }
    }
    var detailViewController: DetailViewController? = nil
    
    deinit {
        stopObservingViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let viewModel = viewModel {
            self.navigationItem.title = viewModel.name
        }
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        let notificationCenter = NSNotificationCenter.defaultCenter()
        func visibleCellConfiguratorForTwoUp(twoUp: Bool) -> ((NSNotification) -> ()) {
            return { [weak self] _ in
                for cell in self?.tableView?.visibleCells ?? [] {
                    self?.configureCell(cell, forTwoUp: twoUp)
                }
            }
        }
        notificationCenter.addObserverForName(AppConstants.SplitViewWillCollapseNotification, object: nil, queue: nil, usingBlock: visibleCellConfiguratorForTwoUp(false))
        notificationCenter.addObserverForName(AppConstants.SplitViewWillSeparateNotification, object: nil, queue: nil, usingBlock: visibleCellConfiguratorForTwoUp(true))
    }
    
    override func viewWillAppear(animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.resignFirstResponder()
    }
    
    override var undoManager: NSUndoManager? {
        return self.viewIfLoaded?.window?.undoManager
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func insertNewObject(sender: AnyObject) {
        viewModel!.insertNewNode()
    }
    
    // MARK: - Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            let indexPath: NSIndexPath
            if let tableViewCell = sender as? UITableViewCell, let senderPath =  self.tableView.indexPathForCell(tableViewCell) {
                indexPath = senderPath
            } else if let selectedPath = self.tableView.indexPathForSelectedRow {
                indexPath = selectedPath
            } else {
                finishPorting("how can we not have a path here?")
            }
            
            let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
            var newViewModel = viewModel!
            try! newViewModel.moveDownAtIndex(indexPath.row)  // CCC, 10/17/2015. Expect this to never fail, but should we have error handling to avoid the crash
            controller.viewModel = newViewModel.detailViewModel
            controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }
    
    // MARK: - Table View
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.countOfChildren ?? 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        configureCell(cell, forTwoUp: isTwoUp)
        cell.textLabel!.text = viewModel!.detailViewModelForChildAtIndex(indexPath.row).name
        return cell
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            try! viewModel!.deleteNodeAtIndex(indexPath.row) // CCC, 10/17/2015. error handling?
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // We always push a new master. Additionally, if we're showing both detail and master, then show new detail as well.
        let newMasterController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("masterViewController") as! MasterViewController
        if var newLens = self.viewModel {
            try! newLens.moveDownAtIndex(indexPath.row) // CCC, 10/18/2015. error handling?
            newMasterController.viewModel = newLens
            self.navigationController?.pushViewController(newMasterController, animated: true)
        }
        
        if isTwoUp {
            self.performSegueWithIdentifier("showDetail", sender: self)
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    //MARK: - Private API
    
    private var isTwoUp: Bool {
        guard let splitViewController = self.splitViewController else {
            return false
        }
        return !splitViewController.collapsed
    }
    
    private func configureCell(cell: UITableViewCell, forTwoUp twoUp: Bool) {
        if twoUp {
            // capable of showing both halves of split view: selection action = (push master + show detail),
            cell.accessoryType = .DisclosureIndicator
        } else {
            // just showing master: selection action = push master, accessory action = show detail
            cell.accessoryType = .DetailDisclosureButton
        }
    }
    
    private func startObservingViewModelIfNeeded() {
        guard registeredViewModelObserverID == nil else {
            // already observing
            return
        }
        guard let viewModel = self.viewModel else {
            // nothing to observe
            return
        }
        
        registeredViewModelObserverID = viewModel.addObserverForChange { [weak self] maybeNewLens in
            if let newLens = maybeNewLens {
                self?.viewModel = newLens
            } else {
                // Need to deregister our observer in case we don't get a new viewModel
                self?.stopObservingViewModel()
                // Should only get here for a non-root master that's been deleted from elsewhere. So, pop until we get to a master that has a non-nil viewModel.
                assert(self?.navigationController?.viewControllers.count > 1, "Lost the view model for the root-level master view controller")
                self?.navigationController?.popViewControllerAnimated(true)
                self?.viewModel = nil
            }
        }
    }
    
    private func stopObservingViewModel() {
        guard let observerID = registeredViewModelObserverID, let viewModel = viewModel else {
            return
        }
        viewModel.removeObserver(observerID)
        registeredViewModelObserverID = nil
    }
    
    private func updateForChangeFromViewModel(previousModel: ListShedMasterViewModel?, toViewModel newModel: ListShedMasterViewModel?) {
        self.navigationItem.title = newModel?.name ?? ""
        // CCC, 10/17/2015. diff models and do appropriate update
        tableView.reloadData()
    }
}

