//
//  DetailViewController.swift
//  ListShed
//

import UIKit

class DetailViewController: UIViewController {
    private var outletsSet: Bool = false
    private var registeredViewModelObserverID: ObserverID?
    
    @IBOutlet weak var nameField: UITextField! {
        didSet {
            outletsSet = true
        }
    }
    @IBOutlet weak var editingControlsContainerView: UIView!
    @IBOutlet weak var noItemSelectedMessageLabel: UILabel!

    var viewModel: ListShedDetailViewModel? = nil {
        didSet {
            startObservingViewModelIfNeeded()
            self.configureView()
        }
    }
    
    //MARK: -
    
    deinit {
        stopObservingViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.resignFirstResponder()
    }
    
    override var undoManager: NSUndoManager? {
        return self.viewIfLoaded?.window?.undoManager
    }

    //MARK: -
    
    @IBAction func nameChanged(sender: UITextField) {
        if var viewModel = viewModel, let text = sender.text {
            viewModel.name = text
        }
    }
    
    // CCC, 10/17/2015. Just a hack to push changes to the model without moving back or adding fields
    @IBAction func saveToModel(sender: AnyObject) {
        self.becomeFirstResponder()
    }
    
    @IBAction func goBack() {
        navigationController?.popViewControllerAnimated(true)
    }

    //MARK: - Private API
    
    private func configureView() {
        guard outletsSet else {
            // Should get kicked again after initialization is complete
            return
        }
        if let viewModel = viewModel {
            // Update the user interface for the detail item.
            nameField.text = viewModel.name
            editingControlsContainerView!.hidden = false
            noItemSelectedMessageLabel!.hidden = true
        } else {
            nameField.text = ""
            editingControlsContainerView!.hidden = true
            noItemSelectedMessageLabel!.hidden = false
        }
    }
    
    private func startObservingViewModelIfNeeded() {
        guard registeredViewModelObserverID == nil else {
            // already observing
            return
        }
        guard let viewModel = self.viewModel else {
            // nothing to observe
            return
        }
        
        registeredViewModelObserverID = viewModel.addObserverForChange { [weak self] maybeNewLens in
            if let newLens = maybeNewLens {
                self?.viewModel = newLens
            } else {
                // Need to deregister our observer in case we don't get a new viewModel
                self?.stopObservingViewModel()
                self?.viewModel = nil
            }
        }
    }
    
    private func stopObservingViewModel() {
        guard let observerID = registeredViewModelObserverID, let viewModel = viewModel else {
            return
        }
        viewModel.removeObserver(observerID)
        registeredViewModelObserverID = nil
    }
}

