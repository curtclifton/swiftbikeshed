//
//  Observatory.swift
//  ListShed
//
//  Created by Curt Clifton on 10/22/15.
//  Copyright © 2015 The Omni Group. All rights reserved.
//

import Foundation

// -------------------------------------------------------------------------
// This was a bad idea. The chain of transformers in the observations has to be on a per-observing class basis. It makes no sense for a value type to be an observer, since we don't have a sensible mechanism to unregister or to control which copy of the struct gets a callback. The implementation on the master branch works, because we're really just having the view controller instances observe the model singleton instance. Chaining through the various structs in between is just an information hiding and type transformation thing. The callback blocks are reference types, and the notification graph is maintained with them.
// -------------------------------------------------------------------------

// CCC, 10/22/2015. not thread safe, but since underlying structures are value types, we can probably make it so
class Observatory {
    struct ObservationTableEntry {
        let observationID: ObservationID
        let observation: Any -> ()
    }
    
    private var observationTable: [ObservableID: [ObservationTableEntry]] = [:]
    
    // CCC, 10/22/2015. might want to add something like notification name, but not stringly typed?
    func newObserverFor<Observed: Observable>(observed:Observed, observer: (Observed?) -> ()) -> ObservationID {
        // CCC, 10/22/2015. need to get a block from observed that lets us refocus, then make and store a callback that will refocus the new observed, also need to get an id from the observed that lets us find the callbacks
        let observedID = observed.observedID
        let reconstructionBlock = observed.rebuilder
        let observationID = ObservationID()
        let blockToStore: Observed -> () = { newObservable in
            let maybeUpdatedNewObservable = reconstructionBlock(newObservable)
            observer(maybeUpdatedNewObservable)
        }
        let entry = ObservationTableEntry(observationID: observationID, observation: blockToStore as! Any -> ())
        if observationTable[observedID] == nil {
            observationTable[observedID] = []
        }
        observationTable[observedID]?.append(entry)
        return observationID
    }
    
    func removeObserverWithID(observationID: ObservationID) {
        // CCC, 10/22/2015. pricey:
        for (observableID, entries) in observationTable {
            observationTable[observableID] = entries.filter { $0.observationID != observationID }
        }
    }
    
    // CCC, 10/22/2015. might want to add something like notification name, but not stringly typed?
    func postNotificationFromObserved<Observed: Observable>(observed: Observed) {
        let observedID = observed.observedID
        guard let entries = observationTable[observedID] else {
            return
        }
        
        for entry in entries {
            entry.observation(observed)
        }
    }
}

typealias ObservableID = String  // CCC, 10/22/2015. better type?
protocol Observable {
    var observedID: ObservableID { get }
    var rebuilder: Self -> Self? { get } // CCC, 10/22/2015. better type
}

extension Observable {
    var rebuilder: Self -> Self? {
        return { $0 }
    }
}

public struct ObservationID {
    private static var nextObservationID = 1
    
    private let id: Int
    
    init() {
        self.id = ObservationID.nextObservationID
        ObservationID.nextObservationID++
    }
}

extension ObservationID: Hashable {
    public var hashValue: Int {
        return id
    }
}

public func ==(lhs: ObservationID, rhs: ObservationID) -> Bool {
    return lhs.id == rhs.id
}

