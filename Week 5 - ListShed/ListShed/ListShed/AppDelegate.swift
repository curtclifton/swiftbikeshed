//
//  AppDelegate.swift
//  ListShed
//

import UIKit

let listShedModel = ListShedModel()

struct AppConstants {
    static let SplitViewWillCollapseNotification = "SplitViewWillCollapseNotification"
    static let SplitViewWillSeparateNotification = "SplitViewWillSeparateNotification"
}

class OperationInjector {
    static let delayTime = 10
    var queue: NSOperationQueue
    
    init() {
        queue = NSOperationQueue()
        queue.maxConcurrentOperationCount = 1
        enqueueMutation()
    }
    
    func enqueueMutation() {
        queue.addOperationWithBlock {
            sleep(UInt32(OperationInjector.delayTime))
            NSOperationQueue.mainQueue().addOperationWithBlock {
                print("tick")
                // CCC, 10/19/2015. tweak the model here
//                listShedModel.rootNode = TreeNode(payload: ListShedPayload())
                self.enqueueMutation()
            }
        }
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?
    var operationInjector: OperationInjector?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        listShedModel.undoManager = self.window!.undoManager
        application.applicationSupportsShakeToEdit = true
        
        let splitViewController = self.window!.rootViewController as! UISplitViewController
        let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as! UINavigationController
        navigationController.topViewController!.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
        splitViewController.delegate = self
        
        // CCC, 10/17/2015. ick ick ick. Do we really have to reach into the view controller hierarchy to get at all of this?
        let rootMasterViewController = splitViewController.viewControllers[0].childViewControllers[0] as! MasterViewController
        let rootLens = TreeLens(model: listShedModel)
        rootMasterViewController.viewModel = ListShedMasterViewModel(lens: rootLens)
        
        operationInjector = OperationInjector()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: - Split view

    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController, ontoPrimaryViewController primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailViewController else { return false }
        
        defer {
            NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: AppConstants.SplitViewWillCollapseNotification, object: nil))
        }
        
        if topAsDetailController.viewModel == nil {
            // This happens on launch in portrait on iPhone 6+ and in all orientations on other iPhones. Since we haven't selected an item in the hierarchy, there's nothing to edit in the detail view controller. Thus, we want to show the root master view controller.
            return true
        }
        return false
    }

    func splitViewController(splitViewController: UISplitViewController, separateSecondaryViewControllerFromPrimaryViewController primaryViewController: UIViewController) -> UIViewController? {
        defer {
            NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: AppConstants.SplitViewWillSeparateNotification, object: nil))
        }
        
        switch (primaryViewController as? UINavigationController)?.topViewController {
        case let masterController as MasterViewController:
            // If master view controller is on top of stack, we need to create and return a detail view controller here to show in the detail pane.
            let detailNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("detailNavigationController") as! UINavigationController
            let detailViewController = detailNavigationController.topViewController as! DetailViewController
            detailViewController.viewModel = masterController.viewModel?.detailViewModel
            return detailNavigationController
        default:
            return nil
        }
    }

}

