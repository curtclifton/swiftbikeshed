//
//  LangValue.swift
//  LangValue
//
//  Created by Curt Clifton on 9/26/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

// Option 2: LangValue as a single struct

struct LangValue {
    let type: LangValueType
    var integerPayload: Int?
    var stringPayload: String?
    var dictionaryPayload: [String: LangValue]?
    
    init(integerValue: Int) {
        self.type = LangValueType.Integer
        self.integerPayload = integerValue
    }
    
    init(stringValue: String) {
        self.type = LangValueType.String
        self.stringPayload = stringValue
    }
    
    init() {
        self.type = LangValueType.Table
        self.dictionaryPayload = [:]
    }
    
    private func argumentError(callingFunction: String = __FUNCTION__) -> LangCoercionError {
        return LangCoercionError.InvalidArgument(message: "cannot invoke “\(callingFunction)” on value of type “\(type)”")
    }

    func integerValue() throws -> Int {
        switch type {
        case .Integer:
            return integerPayload!
        case .String:
            return Int(stringPayload!) ?? 0
        default:
            throw argumentError()
        }
    }
    
    func stringValue() throws -> String {
        switch type {
        case .Integer:
            return String(integerPayload!)
        case .String:
            return stringPayload!
        default:
            throw argumentError()
        }
    }
    
    func valueByAddingValue(value: LangValue) throws -> LangValue {
        switch type {
        case .Integer:
            let resultInt = try integerPayload! + value.integerValue()
            return LangValue(integerValue: resultInt)
        case .String:
            let resultString = try stringPayload! + value.stringValue()
            return LangValue(stringValue: resultString)
        default:
            throw argumentError()
        }
    }

    mutating func setObjectForKey(object: LangValue, key: String) throws {
        guard type == .Table else {
            throw argumentError()
        }
        dictionaryPayload![key] = object
    }
    
    mutating func removeObjectForKey(key: String) throws {
        guard type == .Table else {
            throw argumentError()
        }
        dictionaryPayload![key] = nil
    }
    
    func objectForKey(key: String) throws -> LangValue? {
        guard type == .Table else {
            throw argumentError()
        }
        return dictionaryPayload![key]
    }
    
    func keys() throws -> [String] {
        guard type == .Table else {
            throw argumentError()
        }
        return Array(dictionaryPayload!.keys)
    }
}

extension LangValue: Hashable {
    var hashValue: Int {
        switch type {
        case .Integer:
            return integerPayload!.hashValue
        case.String:
            return stringPayload!.hashValue
        case .Table:
            let result = dictionaryPayload!.keys.reduce(0) { $0 ^ $1.hashValue }
            return result
        }
    }
}

func ==(lhs: LangValue, rhs: LangValue) -> Bool {
    switch (lhs.type, rhs.type) {
    case (.Integer, .Integer):
        return lhs.integerPayload == rhs.integerPayload
    case (.String, .String):
        return lhs.stringPayload == rhs.stringPayload
    case (.Table, .Table):
        return lhs.dictionaryPayload! == rhs.dictionaryPayload!
    default:
        return false
    }
}

// Constructors
func LangValueWithInteger(integer: Int) -> LangValue {
    return LangValue(integerValue: integer)
}

func LangValueWithString(string: String) -> LangValue {
    return LangValue(stringValue: string)
}

func EmptyLangValueTable() -> LangValue {
    return LangValue()
}

// Sequence sums
func addIntegerValuesInArray(values: [LangValue]) throws -> LangValue {
    let intValue = try values.reduce(0) { return try $0 + $1.integerValue() }
    return LangValueWithInteger(intValue)
}

func addStringValuesInArray(values: [LangValue]) throws -> LangValue {
    let stringValue = try values.reduce("") { return try $0 + $1.stringValue() }
    return LangValueWithString(stringValue)
}

// Key sets
// With a single struct, we can make LangValue Hashable. Nice.
func recursiveSetOfStringsInTable(table: LangValue) throws -> Set<LangValue> {
    var result: Set<LangValue> = Set()
    
    func valuesOfType(type: LangValueType, table: LangValue) throws -> [LangValue] {
        let result: [LangValue] = try table.keys().flatMap { (key: String) throws -> LangValue? in
            guard let value = try table.objectForKey(key) else {
                return nil
            }
            return (value.type == type) ? value : nil
        }
        return result
    }
    
    var tablesToScan = ArraySlice([table])
    repeat {
        let tableToScan = tablesToScan.first! // non-empty by precondition and loop condition
        tablesToScan = tablesToScan.dropFirst()
        try tablesToScan.appendContentsOf(valuesOfType(.Table, table: tableToScan))
        let stringValues = try valuesOfType(.String, table: tableToScan)
        result.unionInPlace(stringValues)
    } while !tablesToScan.isEmpty
        
    return result
}



