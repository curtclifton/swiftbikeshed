//
//  Utilities.swift
//  LangValue
//
//  Created by Curt Clifton on 9/26/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

enum LangValueType {
    case Integer
    case String
    case Table
}

enum LangCoercionError: ErrorType {
    case InvalidArgument(message: String)
}

