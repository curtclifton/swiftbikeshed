//
//  LangValue.swift
//  LangValue
//
//  Created by Curt Clifton on 9/26/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

// Option 1: LangValue as an enum. Curious.

enum LangValue {
    case IntegerValue(value: Int)
    case StringValue(value: String)
    case TableValue(value: [String: LangValue])
    
    var type: LangValueType {
        switch self {
        case .IntegerValue(_):
            return .Integer
        case .StringValue(_):
            return .String
        case .TableValue(_):
            return .Table
        }
    }
    
    private func argumentError(callingFunction: String = __FUNCTION__) -> LangCoercionError {
        return LangCoercionError.InvalidArgument(message: "cannot invoke “\(callingFunction)” on value of type “\(self.dynamicType)”")
    }
    
    
    func integerValue() throws -> Int {
        switch self {
        case .IntegerValue(let value):
            return value
        case .StringValue(let value):
            return Int(value) ?? 0
        default:
            throw argumentError()
        }
    }
    
        func stringValue() throws -> String {
            switch self {
            case .IntegerValue(let value):
                return String(value)
            case .StringValue(let value):
                return value
            default:
                throw argumentError()
            }
        }

        func valueByAddingValue(otherValue: LangValue) throws -> LangValue {
            switch self {
            case .IntegerValue(let value):
                let resultInt = try value + otherValue.integerValue()
                return LangValue.IntegerValue(value: resultInt)
            case .StringValue(let value):
                let resultString = try value + otherValue.stringValue()
                return LangValue.StringValue(value: resultString)
            default:
                throw argumentError()
            }
        }
    
        mutating func setObjectForKey(object: LangValue, key: String) throws {
            guard case .TableValue(let value) = self else {
                throw argumentError()
            }
            var newValue = value
            newValue[key] = object
            self = LangValue.TableValue(value: newValue)
        }
    
        mutating func removeObjectForKey(key: String) throws {
            guard case .TableValue(let value) = self else {
                throw argumentError()
            }
            var newValue = value
            newValue[key] = nil
            self = LangValue.TableValue(value: newValue)
        }
    
        func objectForKey(key: String) throws -> LangValue? {
            guard case .TableValue(let value) = self else {
                throw argumentError()
            }
            return value[key]
        }
    
        func keys() throws -> [String] {
            guard case .TableValue(let value) = self else {
                throw argumentError()
            }
            return Array(value.keys)
        }

}

extension LangValue: Hashable {
    var hashValue: Int {
        switch self {
        case .IntegerValue(let value):
            return value.hashValue
        case.StringValue(let value):
            return value.hashValue
        case .TableValue(let value):
            let result = value.keys.reduce(0) { $0 ^ $1.hashValue }
            return result
        }
    }
}

func ==(lhs: LangValue, rhs: LangValue) -> Bool {
    switch (lhs, rhs) {
    case let (.IntegerValue(lhsValue), .IntegerValue(rhsValue)):
        return lhsValue == rhsValue
    case let (.StringValue(lhsValue), .StringValue(rhsValue)):
        return lhsValue == rhsValue
    case let (.TableValue(lhsValue), .TableValue(rhsValue)):
        return lhsValue == rhsValue
    default:
        return false
    }
}

//// Constructors
func LangValueWithInteger(integer: Int) -> LangValue {
    return LangValue.IntegerValue(value: integer)
}

func LangValueWithString(string: String) -> LangValue {
    return LangValue.StringValue(value: string)
}

func EmptyLangValueTable() -> LangValue {
    return LangValue.TableValue(value: [:])
}

// Sequence sums
func addIntegerValuesInArray(values: [LangValue]) throws -> LangValue {
    let intValue = try values.reduce(0) { return try $0 + $1.integerValue() }
    return LangValueWithInteger(intValue)
}

func addStringValuesInArray(values: [LangValue]) throws -> LangValue {
    let stringValue = try values.reduce("") { return try $0 + $1.stringValue() }
    return LangValueWithString(stringValue)
}

// Key sets
// With a single struct, we can make LangValue Hashable. Nice.
func recursiveSetOfStringsInTable(table: LangValue) throws -> Set<LangValue> {
    var result: Set<LangValue> = Set()
    
    func valuesOfType(type: LangValueType, table: LangValue) throws -> [LangValue] {
        let result: [LangValue] = try table.keys().flatMap { (key: String) throws -> LangValue? in
            guard let value = try table.objectForKey(key) else {
                return nil
            }
            return (value.type == type) ? value : nil
        }
        return result
    }
    
    var tablesToScan = ArraySlice([table])
    repeat {
        let tableToScan = tablesToScan.first! // non-empty by precondition and loop condition
        tablesToScan = tablesToScan.dropFirst()
        try tablesToScan.appendContentsOf(valuesOfType(.Table, table: tableToScan))
        let stringValues = try valuesOfType(.String, table: tableToScan)
        result.unionInPlace(stringValues)
    } while !tablesToScan.isEmpty
        
    return result
}



