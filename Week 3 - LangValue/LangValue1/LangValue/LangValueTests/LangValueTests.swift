//
//  LangValueTests.swift
//  LangValueTests
//
//  Created by Curt Clifton on 9/26/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import XCTest
@testable import LangValue

class LangValueTests: XCTestCase {
    let tenInt = LangValueWithInteger(10)
    let twentyInt = LangValueWithInteger(20)
    let fifteenString = LangValueWithString("15")
    let twentyFiveString = LangValueWithString("25")
    let someString = LangValueWithString("some string")
    var table = EmptyLangValueTable()
    
    override func setUp() {
        super.setUp()
        table = EmptyLangValueTable()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    private func expectNoError(@noescape block: () throws -> ()) {
        do {
            try block()
        } catch {
            XCTFail("unexpected error thrown: \(error)")
        }
    }
    
    private func expectError(@noescape block: () throws -> ()) {
        do {
            try block()
            XCTFail("expected an error")
        } catch {
            // yay!
        }
    }
    
    func testLangValueIntIntegerValue() {
        expectNoError {
            let ten = try tenInt.integerValue()
            let twenty = try twentyInt.integerValue()
            XCTAssertEqual(ten, 10)
            XCTAssertEqual(twenty, 20)
        }
    }
    
    func testLangValueStringIntegerValue() {
        expectNoError {
            let fifteen = try fifteenString.integerValue()
            let twentyFive = try twentyFiveString.integerValue()
            let zero = try someString.integerValue()
            XCTAssertEqual(fifteen, 15)
            XCTAssertEqual(twentyFive, 25)
            XCTAssertEqual(zero, 0)
        }
    }
    
    func testLangValueTableIntegerValue() {
        expectError {
            try table.integerValue()
        }
    }
    
    func testLangValueIntStringValue() {
        expectNoError {
            let ten = try tenInt.stringValue()
            let twenty = try twentyInt.stringValue()
            XCTAssertEqual(ten, "10")
            XCTAssertEqual(twenty, "20")
        }
    }
    
    func testLangValueStringStringValue() {
        expectNoError {
            let fifteen = try fifteenString.stringValue()
            let twentyFive = try twentyFiveString.stringValue()
            let zero = try someString.stringValue()
            XCTAssertEqual(fifteen, "15")
            XCTAssertEqual(twentyFive, "25")
            XCTAssertEqual(zero, "some string")
        }
    }
    
    func testLangValueTableStringValue() {
        expectError {
            try table.stringValue()
        }
    }
    
    func testLangValueIntValueByAddingValue() {
        expectNoError {
            let sum1 = try tenInt.valueByAddingValue(twentyInt).integerValue()
            XCTAssertEqual(sum1, 30)
            
            let sum2 = try tenInt.valueByAddingValue(twentyFiveString).integerValue()
            XCTAssertEqual(sum2, 35)
        }
        
        expectError {
            try tenInt.valueByAddingValue(table)
        }
    }
    
    func testLangValueStringValueByAddingValue() {
        expectNoError {
            let concatenation1 = try fifteenString.valueByAddingValue(tenInt).stringValue()
            XCTAssertEqual(concatenation1, "1510")
            
            let concatenation2 = try fifteenString.valueByAddingValue(twentyFiveString).stringValue()
            XCTAssertEqual(concatenation2, "1525")
        }
        
        expectError {
            try fifteenString.valueByAddingValue(table)
        }
    }
    
    func testLangValueTableValueByAddingValue() {
        expectError {
            try table.valueByAddingValue(tenInt)
        }
        expectError {
            try table.valueByAddingValue(fifteenString)
        }
        expectError {
            try table.valueByAddingValue(table)
        }
    }

    func testTableMethodsOnLangValueInt() {
        var receiver = tenInt
        expectError { try receiver.setObjectForKey(tenInt, key: "ten") }
        receiver = tenInt
        expectError { try receiver.removeObjectForKey("ten") }
        expectError { try tenInt.objectForKey("ten") }
        expectError { try tenInt.keys() }
    }
    
    func testTableMethodsOnLangValueString() {
        var receiver = fifteenString
        expectError { try receiver.setObjectForKey(tenInt, key: "ten") }
        receiver = fifteenString
        expectError { try receiver.removeObjectForKey("ten") }
        expectError { try fifteenString.objectForKey("ten") }
        expectError { try fifteenString.keys() }
    }
    
    func testTableMethodsOnLangValueTable() {
        var receiver = table

        func keysShouldBe(expected: [String]) throws {
            let keys = try Set(receiver.keys())
            XCTAssertEqual(keys, Set(expected))
        }
        
        expectNoError {
            try keysShouldBe([])
            
            try receiver.setObjectForKey(tenInt, key: "ten")
            try keysShouldBe(["ten"])

            guard let value = try receiver.objectForKey("ten") else {
                XCTFail("expected non-nil value");
                return
            }
            let intValue = try value.integerValue()
            XCTAssertEqual(intValue, 10)

            try receiver.removeObjectForKey("ten")
            try keysShouldBe([])
            let value2 = try receiver.objectForKey("ten")
            XCTAssertNil(value2);
            
            try receiver.setObjectForKey(fifteenString, key: "fifteen")
            try receiver.setObjectForKey(twentyFiveString, key: "twenty five")
            try keysShouldBe(["twenty five", "fifteen"])
        }
    }
    
    // First test from problem statement
    func testSequenceSums() {
        expectNoError {
            let s = LangValueWithString("This is a string")
            let n = LangValueWithInteger(42)
            let someValues = [s, n]
            let intResult = try addIntegerValuesInArray(someValues).integerValue()
            XCTAssertEqual(intResult, 42)
            
            let stringResult = try addStringValuesInArray(someValues).stringValue()
            XCTAssertEqual(stringResult, "This is a string42")
            
            let unknownResult = try s.valueByAddingValue(n)
            XCTAssertEqual(unknownResult.type, LangValueType.String)
        }
    }
    
    // Second test from problem statement
    func testTablesAndSets() {
        expectNoError {
            var t = table
            try t.setObjectForKey(LangValueWithInteger(10), key: "SomeInt")
            let someString = LangValueWithString("Some string")
            try t.setObjectForKey(someString, key: "SomeString")
            
            var subtable = table
            try subtable.setObjectForKey(LangValueWithInteger(50), key: "SubtableInt")
            let anotherString = LangValueWithString("Another string")
            try subtable.setObjectForKey(anotherString, key: "SubtableString")
            
            try t.setObjectForKey(subtable, key: "Subtable")
            
            let setOfStrings = try recursiveSetOfStringsInTable(t)
            let expectedResults: [LangValue] = [someString, anotherString]
            XCTAssertEqual(setOfStrings, Set(expectedResults))
        }
    }
}
