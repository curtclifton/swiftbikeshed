//
//  main.swift
//  LangValue, http://inessential.com/langvalue
//
//  Created by Curt Clifton on 9/26/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

func main() throws {
    let i = 10
    let j = 20
    print(try i.integerValue())
    print(try i.stringValue())
    print(try i.valueByAddingValue(j))
    
    let s = "15"
    let t = "25"
    let u = "some string"
    print(try s.integerValue())
    print(try t.integerValue())
    print(try u.integerValue())
    
    print(try s.stringValue())
    print(try s.valueByAddingValue(t))
    
    print(try i.valueByAddingValue(s))
    print(try s.valueByAddingValue(i))
    
    var nastyTable = [ 1: ["dog"], 2: ["cat"], 3: ["horse"] ] // bad Key and Value types
    var alsoNastyTable = [ 1: "dog", 2: "cat", 3: "horse" ] // Value type is OK, but Key type is bad
    var niceTable = ["dog": 1, "cat": 2, "horse": 3]
    
    try nastyTable.setObjectForKey("object", key: "key")
    try alsoNastyTable.setObjectForKey("object", key: "key")
    
    print(niceTable)
    try niceTable.setObjectForKey("object", key: "key")
    print(niceTable)
}

do {
    try main()
} catch {
    print(error)
}