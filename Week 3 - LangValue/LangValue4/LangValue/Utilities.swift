import Foundation

enum LangValueType {
    case Integer
    case String
    case Table
}

enum LangCoercionError: ErrorType {
    case InvalidArgument(message: String)
}

