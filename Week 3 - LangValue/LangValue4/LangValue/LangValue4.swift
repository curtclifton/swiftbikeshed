import Foundation

// Option 4: LangValue as protocol plus extensions to built-in types.

protocol LangValue {
    var type: LangValueType { get }
    
    func integerValue() throws -> Int
    func stringValue() throws -> String
    func valueByAddingValue(value: LangValue) throws -> LangValue
    
    mutating func setObjectForKey(object: LangValue, key: String) throws
    mutating func removeObjectForKey(key: String) throws
    func objectForKey(key: String) throws -> LangValue?
    func keys() throws -> [String]
}

// Provide default, throwing implementations of everything
extension LangValue {
    func argumentError(callingFunction: String = __FUNCTION__) -> LangCoercionError {
        return LangCoercionError.InvalidArgument(message: "cannot invoke “\(callingFunction)” on value of type “\(self.dynamicType)”")
    }
    
    func integerValue() throws -> Int {
        throw argumentError()
    }
    
    func stringValue() throws -> String {
        throw argumentError()
    }
    
    func valueByAddingValue(value: LangValue) throws -> LangValue {
        throw argumentError()
    }

    mutating func setObjectForKey(object: LangValue, key: String) throws {
        throw argumentError()
    }
    
    mutating func removeObjectForKey(key: String) throws {
        throw argumentError()
    }
    
    func objectForKey(key: String) throws -> LangValue? {
        throw argumentError()
    }
    
    func keys() throws -> [String] {
        throw argumentError()
    }
}

extension Int: LangValue {
    var type: LangValueType {
        return .Integer
    }
    
    func integerValue() throws -> Int {
        return self
    }
    
    func stringValue() throws -> String {
        return String(self)
    }
    
    func valueByAddingValue(value: LangValue) throws -> LangValue {
        let otherValue = try value.integerValue()
        return self + otherValue
    }
}

extension String: LangValue {
    var type: LangValueType {
        return .String
    }
    
    func integerValue() throws -> Int {
        guard let result = Int(self) else {
            return 0
        }
        return result
    }
    
    func stringValue() throws -> String {
        return self
    }
    
    func valueByAddingValue(value: LangValue) throws -> LangValue {
        let otherValue = try value.stringValue()
        return self + otherValue
    }
}

// Make every Dictionary conform to LangValue, but use default throwing implementations. We'll override below to do the right thing with Dictionaries that have the correct "shape".
extension Dictionary: LangValue {
    var type: LangValueType {
        return .Table
    }
}

// Unfortunately, Swift doesn't (yet?) let us write an extension that constrains a generic parameter to a concrete type, so we can't make Key == String.
// <unknown>:0: error: type 'Key' constrained to non-protocol type 'String'
extension Dictionary where Value: LangValue, Key: String {
    mutating func setObjectForKey(object: LangValue, key: String) throws {
        self[key] = object
    }
    
    mutating func removeObjectForKey(key: String) throws {
        self[key] = nil
    }
    
    func objectForKey(key: String) throws -> LangValue? {
        return self[key]
    }
    
    func keys() throws -> [String] {
        return keys
    }
}


