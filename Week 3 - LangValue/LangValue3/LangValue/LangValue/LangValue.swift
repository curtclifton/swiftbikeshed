//
//  LangValue.swift
//  LangValue
//
//  Created by Curt Clifton on 9/26/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

// Option 3: LangValue as protocol with structs implementing the value types

protocol LangValue {
    var type: LangValueType { get }
    
    func integerValue() throws -> Int
    func stringValue() throws -> String
    func valueByAddingValue(value: LangValue) throws -> LangValue
    
    mutating func setObjectForKey(object: LangValue, key: String) throws
    mutating func removeObjectForKey(key: String) throws
    func objectForKey(key: String) throws -> LangValue?
    func keys() throws -> [String]
}

// Provide default, throwing implementations of everything
extension LangValue {
    func argumentError(callingFunction: String = __FUNCTION__) -> LangCoercionError {
        return LangCoercionError.InvalidArgument(message: "cannot invoke “\(callingFunction)” on value of type “\(self.dynamicType)”")
    }
    
    func integerValue() throws -> Int {
        throw argumentError()
    }
    
    func stringValue() throws -> String {
        throw argumentError()
    }
    
    func valueByAddingValue(value: LangValue) throws -> LangValue {
        throw argumentError()
    }
    
    mutating func setObjectForKey(object: LangValue, key: String) throws {
        throw argumentError()
    }
    
    mutating func removeObjectForKey(key: String) throws {
        throw argumentError()
    }
    
    func objectForKey(key: String) throws -> LangValue? {
        throw argumentError()
    }
    
    func keys() throws -> [String] {
        throw argumentError()
    }
}

typealias LangValueInt = Int
extension Int: LangValue {
    var type: LangValueType {
        return .Integer
    }
    
    func integerValue() throws -> Int {
        return self
    }
    
    func stringValue() throws -> String {
        return String(self)
    }
    
    func valueByAddingValue(value: LangValue) throws -> LangValue {
        let otherValue = try value.integerValue()
        return self + otherValue
    }
}

typealias LangValueString = String
extension String: LangValue {
    var type: LangValueType {
        return .String
    }
    
    func integerValue() throws -> Int {
        guard let result = Int(self) else {
            return 0
        }
        return result
    }
    
    func stringValue() throws -> String {
        return self
    }
    
    func valueByAddingValue(value: LangValue) throws -> LangValue {
        let otherValue = try value.stringValue()
        return self + otherValue
    }
}

struct LangValueTable {
    var backingDictionary: [String: LangValue] = [:]
}

extension LangValueTable: LangValue {
    var type: LangValueType {
        return .Table
    }
    
    mutating func setObjectForKey(object: LangValue, key: String) throws {
        backingDictionary[key] = object
    }
    
    mutating func removeObjectForKey(key: String) throws {
        backingDictionary[key] = nil
    }
    
    func objectForKey(key: String) throws -> LangValue? {
        return backingDictionary[key]
    }
    
    func keys() throws -> [String] {
        return Array(backingDictionary.keys)
    }
}

extension LangValueTable: Equatable, Hashable {
    var hashValue: Int {
        return backingDictionary.count
    }
}

func ==(lhs: LangValueTable, rhs: LangValueTable) -> Bool {
    return false
}

// Constructors
func LangValueWithInteger(integer: Int) -> LangValue {
    return integer
}

func LangValueWithString(string: String) -> LangValue {
    return string
}

func EmptyLangValueTable() -> LangValue {
    return LangValueTable()
}

// Sequence sums
func addIntegerValuesInArray(values: [LangValue]) throws -> LangValue {
    let intValue = try values.reduce(0) { return try $0 + $1.integerValue() }
    return LangValueWithInteger(intValue)
}

func addStringValuesInArray(values: [LangValue]) throws -> LangValue {
    let stringValue = try values.reduce("") { return try $0 + $1.stringValue() }
    return LangValueWithString(stringValue)
}

// Key sets
// The original function signature throws a real wrench, because we can't make the protocol Hashable. So, we'll cheat a bit and return a set of LangValueStrings instead of just LangValue.
func recursiveSetOfStringsInTable(table: LangValue) throws -> Set<LangValueString> {
    var result: Set<LangValueString> = Set()
    
    func valuesOfType(type: LangValueType, table: LangValue) throws -> [LangValue] {
        let result: [LangValue] = try table.keys().flatMap { (key: String) throws -> LangValue? in
            guard let value = try table.objectForKey(key) else {
                return nil
            }
            return (value.type == type) ? value : nil
        }
        return result
    }
    
    var tablesToScan = ArraySlice([table])
    repeat {
        let tableToScan = tablesToScan.first! // non-empty by precondition and loop condition
        tablesToScan = tablesToScan.dropFirst()
        try tablesToScan.appendContentsOf(valuesOfType(.Table, table: tableToScan))
        let stringValues = try valuesOfType(.String, table: tableToScan)
        let coercedStringValues = try stringValues.map { try $0.stringValue() }
        result.unionInPlace(coercedStringValues)
    } while !tablesToScan.isEmpty
        
    return result
}



