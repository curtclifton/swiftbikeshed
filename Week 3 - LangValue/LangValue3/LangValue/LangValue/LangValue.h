//
//  LangValue.h
//  LangValue
//
//  Created by Curt Clifton on 9/26/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for LangValue.
FOUNDATION_EXPORT double LangValueVersionNumber;

//! Project version string for LangValue.
FOUNDATION_EXPORT const unsigned char LangValueVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LangValue/PublicHeader.h>


