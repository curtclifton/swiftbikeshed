//
//  Checkout.swift
//  Checkout
//
//  Created by Curt Clifton on 10/22/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

// All prices in cents to keep things simple

typealias SKU = String

func itemsForCart(cart: String) -> [SKU] {
    return cart.characters.map { String($0) }
}

protocol PricingRule {
    var items: CountedSet<SKU> { get }
    var price: Int { get }
}

struct PerItemPricingRule: PricingRule {
    let sku: SKU
    let price: Int
    
    var items: CountedSet<SKU> {
        return [sku]
    }
}

struct MultiItemPricingRule: PricingRule {
    let items: CountedSet<SKU>
    let price: Int
    
    init(sku: SKU, quantity: Int, price: Int) {
        self.items = CountedSet(contents: [sku: quantity])
        self.price = price
    }
}

struct CrossPromotionPricingRule: PricingRule {
    let items: CountedSet<SKU>
    let price: Int
}

struct PricingRules {
    let rules: [PricingRule]
    init(_ rules: [PricingRule]) {
        self.rules = rules
    }
    
    func priceForItems(items: CountedSet<SKU>) -> Int {
        // Knapsack problem! Assume n is bounded by the size of the shopping cart and just brute force it
        let possiblePrices = pricesForItems(items, priceSoFar: 0)
        guard let result = possiblePrices.minElement() else {
            abort()
        }
        return result
    }
    
    private func applicableRulesForItems(itemsRemaining: CountedSet<SKU>) -> [PricingRule] {
        return rules.filter { itemsRemaining.contains($0.items) }
    }
    
    private func pricesForItems(itemsRemaining: CountedSet<SKU>, priceSoFar: Int) -> [Int] {
        if itemsRemaining.isEmpty {
            return [priceSoFar]
        }
        let applicableRules = rules.filter { itemsRemaining.contains($0.items) }
        let partialResult: [(CountedSet<SKU>, Int)] = applicableRules.map { rule in
            var newItemsRemaining = itemsRemaining
            newItemsRemaining.removeElements(rule.items)
            return (newItemsRemaining, rule.price)
        }
        let finalPrices: [Int] = partialResult.flatMap { (itemsRemaining, price) -> [Int] in
            let newPrice = price + priceSoFar
            if itemsRemaining.isEmpty {
                return [newPrice]
            } else {
                return self.pricesForItems(itemsRemaining, priceSoFar: newPrice)
            }
        }
        return finalPrices
    }
}

extension PricingRules: ArrayLiteralConvertible {
    init(arrayLiteral elements: PricingRule...) {
        self.init(elements)
    }
}

struct CheckOut {
    let pricingRules: PricingRules
    var itemSKUs: CountedSet<SKU> = []
    
    init(pricingRules: PricingRules) {
        self.pricingRules = pricingRules
    }
    
    mutating func scan(itemSKU: SKU) {
        itemSKUs.addElement(itemSKU)
    }
    
    func total() -> Int {
        let result = pricingRules.priceForItems(itemSKUs)
        return result
    }
}

// CCC, 10/22/2015. Probably would want this to be a SequenceType
struct CountedSet<Element: Hashable> {
    private var contents: [Element: Int] = [:]
    
    var isEmpty: Bool {
        return contents.isEmpty
    }
    
    var totalCount: Int {
        return contents.reduce(0) { $0 + $1.1 }
    }
    
    mutating func addElement(element: Element) {
        contents[element] = self[element] + 1
    }
    
    /// If element is present in the set, decrements the count associated with it. If the count is decremented to 0, element is removed from the set. `removeObject(_:)` does nothing if element is not present in the set.
    mutating func removeElement(element: Element) {
        let newCount = self[element] - 1
        if newCount > 0 {
            contents[element] = newCount
        } else {
            contents[element] = nil
        }
    }
    
    /// In keeping with the behavior of `removeElement`, does *not* require `contains(otherSet)`.
    mutating func removeElements(otherSet: CountedSet<Element>) {
        for (element, otherCount) in otherSet.contents {
            let newCount = self[element] - otherCount
            if newCount > 0 {
                contents[element] = newCount
            } else {
                contents[element] = nil
            }
        }
    }
    
    func contains(otherSet: CountedSet<Element>) -> Bool {
        for (element, otherCount) in otherSet.contents {
            if self[element] < otherCount {
                return false
            }
        }
        return true
    }
    
    subscript(element: Element) -> Int {
        return contents[element] ?? 0
    }
}

extension CountedSet: ArrayLiteralConvertible {
    init(arrayLiteral elements: Element...) {
        self.init()
        for element in elements {
            addElement(element)
        }
    }
}

extension CountedSet: Equatable {
}

func ==<Element>(lhs: CountedSet<Element>, rhs: CountedSet<Element>) -> Bool {
    // CCC, 10/22/2015. Could make this cheaper by iterating the contents of one set
    return lhs.contains(rhs) && rhs.contains(lhs)
}