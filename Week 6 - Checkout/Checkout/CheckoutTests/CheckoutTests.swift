//
//  CheckoutTests.swift
//  CheckoutTests
//
//  Created by Curt Clifton on 10/22/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import XCTest
@testable import Checkout

class CheckoutTests: XCTestCase {
    var rules: PricingRules = []
    
    override func setUp() {
        super.setUp()
        /*
        Item   Unit      Special
        Price     Price
        --------------------------
        A     50       3 for 130
        B     30       2 for 45
        C     20
        D     15
        */
        rules = [
            PerItemPricingRule(sku: "A", price: 50),
            PerItemPricingRule(sku: "B", price: 30),
            PerItemPricingRule(sku: "C", price: 20),
            PerItemPricingRule(sku: "D", price: 15),
            MultiItemPricingRule(sku: "A", quantity: 3, price: 130),
            MultiItemPricingRule(sku: "B", quantity: 2, price: 45),
        ]
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func price(cart: String) -> Int {
        let items = itemsForCart(cart)
        var checkout = CheckOut(pricingRules: rules)
        for item in items {
            checkout.scan(item)
        }
        return checkout.total()
    }
    
    func testTotals() {
        XCTAssertEqual(  0, price(""))
        XCTAssertEqual( 50, price("A"))
        XCTAssertEqual( 80, price("AB"))
        XCTAssertEqual(115, price("CDBA"))
        
        XCTAssertEqual(100, price("AA"))
        XCTAssertEqual(130, price("AAA"))
        XCTAssertEqual(180, price("AAAA"))
        XCTAssertEqual(230, price("AAAAA"))
        XCTAssertEqual(260, price("AAAAAA"))

        XCTAssertEqual(160, price("AAAB"))
        XCTAssertEqual(175, price("AAABB"))
        XCTAssertEqual(190, price("AAABBD"))
        XCTAssertEqual(190, price("DABABA"))
    }
    
    func testIncremental() {
        var checkout = CheckOut(pricingRules: rules)
        XCTAssertEqual(  0, checkout.total())
        checkout.scan("A");  XCTAssertEqual( 50, checkout.total())
        checkout.scan("B");  XCTAssertEqual( 80, checkout.total())
        checkout.scan("A");  XCTAssertEqual(130, checkout.total())
        checkout.scan("A");  XCTAssertEqual(160, checkout.total())
        checkout.scan("B");  XCTAssertEqual(175, checkout.total())
    }

    func testCrossPromotion() {
        let bAndTwoAs: CountedSet<SKU> = ["B", "A", "A"] // the loud sheep special
        rules = [
            PerItemPricingRule(sku: "A", price: 50),
            PerItemPricingRule(sku: "B", price: 30),
            PerItemPricingRule(sku: "C", price: 20),
            PerItemPricingRule(sku: "D", price: 15),
            MultiItemPricingRule(sku: "A", quantity: 3, price: 130),
            MultiItemPricingRule(sku: "B", quantity: 2, price: 45),
            CrossPromotionPricingRule(items: bAndTwoAs, price: 100)
        ]
        var checkout = CheckOut(pricingRules: rules)
        XCTAssertEqual(  0, checkout.total())
        checkout.scan("A");  XCTAssertEqual( 50, checkout.total())
        checkout.scan("B");  XCTAssertEqual( 80, checkout.total())
        checkout.scan("A");  XCTAssertEqual(100, checkout.total())
        checkout.scan("A");  XCTAssertEqual(150, checkout.total())
        checkout.scan("B");  XCTAssertEqual(175, checkout.total()) // cheaper to switch to the AAA + BB specials instead of BAA + A + B
    }
    
    func testCountedSet() {
        var countedSet: CountedSet<String> = []
        XCTAssert(countedSet.isEmpty)
        XCTAssertEqual(countedSet["A"], 0)
        XCTAssertEqual(countedSet["B"], 0)
        
        countedSet.addElement("A")
        countedSet.addElement("A")
        countedSet.addElement("B")
        XCTAssertFalse(countedSet.isEmpty)
        XCTAssertEqual(countedSet["A"], 2)
        XCTAssertEqual(countedSet["B"], 1)
        
        countedSet.removeElement("A")
        XCTAssertEqual(countedSet["A"], 1)
        XCTAssertEqual(countedSet["B"], 1)
        
        countedSet.removeElement("A")
        XCTAssertEqual(countedSet["A"], 0)
        XCTAssertEqual(countedSet["B"], 1)
        
        countedSet.removeElement("A")
        XCTAssertEqual(countedSet["A"], 0)
        XCTAssertEqual(countedSet["B"], 1)
        
        countedSet.removeElement("B")
        XCTAssert(countedSet.isEmpty)
        XCTAssertEqual(countedSet["A"], 0)
        XCTAssertEqual(countedSet["B"], 0)
        
        let countedSet2: CountedSet<String> = ["A", "B", "A"]
        XCTAssertEqual(countedSet2["A"], 2)
        XCTAssertEqual(countedSet2["B"], 1)
        
        var countedSet3 = countedSet2
        countedSet3.addElement("B")
        XCTAssert(countedSet2.contains(countedSet))
        XCTAssertFalse(countedSet2.contains(countedSet3))
        XCTAssert(countedSet3.contains(countedSet2))
        
        var countedSet4 = countedSet3
        countedSet4.removeElements(countedSet) // remove nothing
        XCTAssertEqual(countedSet3, countedSet4)
        
        countedSet4.removeElements(countedSet2)
        XCTAssertEqual(countedSet4["A"], 0)
        XCTAssertEqual(countedSet4["B"], 1)
    }
    
}
