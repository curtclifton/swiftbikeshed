//
//  Signal.swift
//  Apollo13
//
//  Created by Curt Clifton on 11/18/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

protocol SignalMappable {
    typealias Payload
    var handlerUpdaters: [Payload -> Void] { get set }
    var value: Payload { get }
    mutating func map<Result>(handler: Payload -> Result) -> Signal<Result>
}

extension SignalMappable {
    mutating func map<Result>(handler: Payload -> Result) -> Signal<Result> {
        let result = handler(value)
        let newSignal = Signal<Result>(initialValue: result)
        
        // Using a closure here to erase the Result type:
        let handlerUpdater: Payload -> Void = { newValue in
            let newResult = handler(newValue)
            newSignal.value = newResult
        }
        handlerUpdaters.append(handlerUpdater)
        
        return newSignal
    }
    
    func broadcastUpdate() {
        for updater in handlerUpdaters {
            updater(value)
        }
    }
}

public class Signal<Payload>: SignalMappable {
    var handlerUpdaters: [Payload -> Void] = []
    var value: Payload {
        didSet {
            broadcastUpdate()
            let sourceArray = sources.allObjects
            for case let source as SignalSource<Payload> in sourceArray {
                source.value = value
            }
            sources.compact()
        }
    }
    
    private var sources: NSPointerArray = NSPointerArray.weakObjectsPointerArray()
    
    init(initialValue: Payload) {
        self.value = initialValue
    }
    
    /// Returns a source instance for this signal. Clients should hold a strong reference to the source to keep it from being deallocated. Changes to the value of this signal appear on all signal sources vended by this method. Clients can unsubscribe from the signal by nilling their reference to their signal source.
    func sourceInstance() -> SignalSource<Payload> {
        let newSource = SignalSource(initialValue: value)
        let newSourcePointer = UnsafeMutablePointer<SignalSource<Payload>>(Unmanaged.passUnretained(newSource).toOpaque())
        sources.addPointer(newSourcePointer)
        return newSource
    }
}

public class SignalSource<Payload>: SignalMappable {
    var handlerUpdaters: [Payload -> Void] = []
    
    private(set) var value: Payload {
        didSet {
            broadcastUpdate()
        }
    }
    
    private init(initialValue: Payload) {
        self.value = initialValue
    }
}