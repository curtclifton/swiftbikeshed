//
//  Apollo13.h
//  Apollo13
//
//  Created by Curt Clifton on 11/18/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for Apollo13.
FOUNDATION_EXPORT double Apollo13VersionNumber;

//! Project version string for Apollo13.
FOUNDATION_EXPORT const unsigned char Apollo13VersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Apollo13/PublicHeader.h>


