//
//  Apollo13Tests.swift
//  Apollo13Tests
//
//  Created by Curt Clifton on 11/18/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import XCTest
@testable import Apollo13

class Apollo13Tests: XCTestCase {
    var intSignal = Signal(initialValue: 0)
    var stringSignal = Signal(initialValue: "")
    
    override func setUp() {
        super.setUp()
        intSignal.value = 0
        stringSignal.value = ""
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialMap() {
        intSignal.map { value in
            XCTAssertEqual(value, 0)
        }
        
        stringSignal.map { value in
            XCTAssertEqual(value, "")
        }
    }
    
    func testUpdate() {
        var resultAccumulator: [Int] = []
        intSignal.map { value in
            resultAccumulator.append(value)
        }
        XCTAssertEqual(resultAccumulator, [0])
        intSignal.value = 1
        XCTAssertEqual(resultAccumulator, [0, 1])
        intSignal.value = 2
        XCTAssertEqual(resultAccumulator, [0, 1, 2])
    }
    
    func testChain() {
        var resultAccumulator: [String] = []
        var outSignal = intSignal.map { value in
            return String(value)
        }
        outSignal.map { value in
            resultAccumulator.append(value)
        }
        XCTAssertEqual(resultAccumulator, ["0"])
        intSignal.value = 1
        XCTAssertEqual(resultAccumulator, ["0", "1"])
        intSignal.value = 2
        XCTAssertEqual(resultAccumulator, ["0", "1", "2"])
    }
    
    func testChainSignalSource() {
        var resultAccumulator: [String] = []
        var intSignalSource = intSignal.sourceInstance()
        let outSignal = intSignalSource.map { value in
            return String(value)
        }
        var outSignalSource = outSignal.sourceInstance()
        outSignalSource.map { value in
            resultAccumulator.append(value)
        }
        XCTAssertEqual(resultAccumulator, ["0"])
        intSignal.value = 1
        XCTAssertEqual(resultAccumulator, ["0", "1"])
        intSignal.value = 2
        XCTAssertEqual(resultAccumulator, ["0", "1", "2"])
    }
    
    func testChainSignalSourceRelease() {
        var resultAccumulator: [String] = []
        var intSignalSource = intSignal.sourceInstance()
        let outSignal = intSignalSource.map { value in
            return String(value)
        }
        
        autoreleasepool {
            var outSignalSource: SignalSource<String>? = outSignal.sourceInstance()
            outSignalSource!.map { value in
                resultAccumulator.append(value)
            }
            XCTAssertEqual(resultAccumulator, ["0"])
            intSignal.value = 1
            XCTAssertEqual(resultAccumulator, ["0", "1"])
        }
        // Exiting autoreleasepool block scope should discard original outSignalSource such that subsequent update to intSignal doesn't affect resultAccumulator
        intSignal.value = 2
        XCTAssertEqual(resultAccumulator, ["0", "1"])
    }
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
