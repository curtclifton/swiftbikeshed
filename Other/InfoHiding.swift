//: #InfoHiding
//: Can we make a dynamic wrapper for a struct that only exposes some properties?

import Foundation

//: First, a some types to play with…
struct WrappedStructThing {
    var name: String
    var count: Int
    var id: String
    
    func moveUp() {
        print("🆙")
    }
}

class WrappedClassThing {
    var name: String
    var count: Int
    var id: String
    
    init(name: String, count: Int, id: String) {
        self.name = name
        self.count = count
        self.id = id
    }
    
    func moveUp() {
        print("🆙")
    }
}


//: It would be cool if `Mirror` gave us some useful hooks, but it seems to only reflect the data and not the operations.
let thing = WrappedStructThing(name: "curt", count: 13, id: "x")
let thingMirror = Mirror(reflecting: thing)
thingMirror.description
thingMirror.displayStyle
thingMirror.children.forEach {
    print($0)
}

let structMoveUpMirror = Mirror(reflecting: WrappedStructThing.moveUp)
structMoveUpMirror.description
structMoveUpMirror.displayStyle
structMoveUpMirror.children.forEach {
    print($0)
}

let otherThing = WrappedClassThing(name: "curt", count: 13, id: "x")
let otherThingMirror = Mirror(reflecting: otherThing)
otherThingMirror.description
otherThingMirror.displayStyle
otherThingMirror.children.forEach {
    print($0)
}

let classMoveUpMirror = Mirror(reflecting: WrappedClassThing.moveUp)
classMoveUpMirror.description
classMoveUpMirror.displayStyle
classMoveUpMirror.children.forEach {
    print($0)
}

//: Can we use an Obj-C wrapper and runtime methods?

@objc class Wrapper: NSObject {
    var wrappedThing: Any
    let legalSelectors: [Selector]
    
    init(wrapping wrappedThing: Any) {
        self.wrappedThing = wrappedThing
        legalSelectors = ["moveUp"]
    }

    override var description: String {
        return "Wrapper around “\(wrappedThing)”"
    }
    
    override func respondsToSelector(aSelector: Selector) -> Bool {
        print("checking: \(aSelector)")
        if legalSelectors.contains(aSelector) {
            return true
        }
        return super.respondsToSelector(aSelector)
    }
    
    override func performSelector(aSelector: Selector) -> Unmanaged<AnyObject>! {
        print("performing: \(aSelector)")
        if legalSelectors.contains(aSelector) {
            let result: Unmanaged<AnyObject>! = Unmanaged.fromOpaque(COpaquePointer(nilLiteral:()))
            return result
        }
        return super.performSelector(aSelector)
    }
}

let structWrapper = Wrapper(wrapping: thing)
structWrapper.moveUp()


