//
//  CardGameModel.swift
//  Klondike
//
//  Created by Curt Clifton on 9/30/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

// The problem: http://codekata.com/kata/kata20-klondike/

enum CardGameError: ErrorType {
    case IllegalMoveError(message: String)
}

enum Color {
    case Red, Black
}

enum Suit: Int {
    case Spades = 0, Clubs, Diamonds, Hearts

    var color: Color {
        switch self {
        case .Spades: fallthrough
        case .Clubs:
            return .Black
        default:
            return .Red
        }
    }
    
    private struct SuitGenerator: GeneratorType {
        private var nextValue: Int = Suit.Spades.rawValue
        mutating func next() -> Suit? {
            let result = Suit(rawValue: nextValue)
            nextValue++
            return result;
        }
    }
    
    static func allSuits() -> AnySequence<Suit> {
        return AnySequence<Suit>( { SuitGenerator() } )
    }
}

enum Rank: Int {
    case Ace = 1
    case Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten
    case Jack, Queen, King
    
    var nextHigherRank: Rank? {
        return Rank(rawValue: self.rawValue + 1)
    }
    
    private struct RankGenerator: GeneratorType {
        private var nextValue: Int = Rank.Ace.rawValue
        mutating func next() -> Rank? {
            let result = Rank(rawValue: nextValue)
            nextValue++
            return result;
        }
    }
    
    static func allRanks() -> AnySequence<Rank> {
        return AnySequence<Rank>( { RankGenerator() } )
    }
}

struct Card {
    let suit: Suit
    let rank: Rank
    
    static func allCards() -> Array<Card> {
        var fullDeck: Array<Card> = []
        for suit: Suit in Suit.allSuits() {
            for rank in Rank.allRanks() {
                fullDeck.append(Card(suit: suit, rank: rank))
            }
        }
        return fullDeck
    }
}

extension Card: Hashable {
    var hashValue: Int {
        return rank.hashValue << 2 + suit.hashValue
    }
}

func ==(leftCard: Card, rightCard: Card) -> Bool {
    return leftCard.rank == rightCard.rank && leftCard.suit == rightCard.suit
}

protocol PileOperationValidator {
    func canAddUnknownCardFaceUp(faceUp: Bool) -> Bool
    /// Default implementation calls canAddCards([card], faceUp: faceUp, toPile: pile)
    func canAddCard(card: Card, faceUp: Bool, toPile pile: Pile) -> Bool
    func canAddCards(cards: [Card], faceUp: Bool, toPile pile: Pile) -> Bool
    func canDrawCards(count: Int, fromPile pile: Pile) -> Bool
    /// Validates revealing of top card
    /// - Parameter pile: it must be the case that `!pile.isEmpty` and `pile.visibleCards.isEmpty`
    /// - Returns: whether it is legal to reveal the top card in the pile
    func canRevealTopCardInPile(pile: Pile) -> Bool
}

// Default implementation is fully permissvie
extension PileOperationValidator {
    func canAddUnknownCardFaceUp(faceUp: Bool) -> Bool {
        return true
    }

    func canAddCard(card: Card, faceUp: Bool, toPile pile: Pile) -> Bool {
        return self.canAddCards([card], faceUp: faceUp, toPile: pile)
    }
    
    func canAddCards(cards: [Card], faceUp: Bool, toPile: Pile) -> Bool {
        return true
    }

    func canDrawCards(count: Int, fromPile: Pile) -> Bool {
        return true
    }
    
    func canRevealTopCardInPile(pile: Pile) -> Bool {
        return true
    }
}

struct PermissivePileOperationValidator: PileOperationValidator {
}

struct Pile {
    private(set) var cards: Array<Card> {
        didSet {
            indexOfTopMostFaceUpCard = min(indexOfTopMostFaceUpCard, cards.count)
        }
    }
    
    private var indexOfTopMostFaceUpCard = 0

    private let validator: PileOperationValidator
    
    /// Creates an empty pile
    init(validator: PileOperationValidator = PermissivePileOperationValidator()) {
        self.cards = []
        self.validator = validator
    }
    
    init(cards: Array<Card>, faceUp: Bool, validator: PileOperationValidator = PermissivePileOperationValidator()) {
        self.cards = cards
        self.validator = validator
        self.indexOfTopMostFaceUpCard = (faceUp ? 0 : cards.count)
    }
    
    var count: Int {
        return cards.count
    }
    
    /// The face-up cards in a pile.
    var visibleCards: [Card] {
        return Array(cards[indexOfTopMostFaceUpCard..<cards.count])
    }
    
    var isEmpty: Bool {
        return cards.isEmpty
    }
    
    mutating func shuffle() {
        cards = cards.shuffled
    }
    
    /// Removes the top card from the pile.
    /// - Returns: the top card
    /// - Throws: `CardGameError.IllegalMoveError` if `isEmpty`
    mutating func drawCard() throws -> Card {
        // CCC, 10/7/2015. validate?
        guard let card = cards.popLast() else {
            throw CardGameError.IllegalMoveError(message: "attempt to draw card from empty pile")
        }
        return card
    }
    
    func canDrawCards(count: Int) -> Bool {
        return cards.count >= count && validator.canDrawCards(count, fromPile: self)
    }
    
    /// Removes the top 'count' cards from the pile.
    /// - Parameter count: the number of cards to draw
    /// - Returns: the top `count` cards in order, with the last card in the result being the former top card on the pile
    /// - Throws: `CardGameError.IllegalMoveError` if `!canDrawCards(count)`
    mutating func drawCards(count: Int) throws -> [Card] {
        guard canDrawCards(count) else {
            throw CardGameError.IllegalMoveError(message: "cannot draw \(count) cards from pile \(self)")
        }
        let resultRange = (cards.count - count)..<cards.count
        let result = Array(cards[resultRange])
        cards.removeRange(resultRange)
        return result;
    }
    
    func canAddUnknownCardFaceUp(faceUp: Bool) -> Bool {
        return validator.canAddUnknownCardFaceUp(faceUp)
    }
    
    func canAddCard(card: Card, faceUp: Bool) -> Bool {
        return validator.canAddCard(card, faceUp: faceUp, toPile: self)
    }
    
    /// Adds the given card to the top of the pile
    /// - Parameter card: the card to add
    /// - Parameter faceUp: whether the card should be added face up
    /// - Throws: `CardGameError.IllegalMoveError` if `!canAddCard(card, faceUp: faceUp)`
    mutating func addCard(card: Card, faceUp: Bool) throws {
        guard canAddCard(card, faceUp: faceUp) else {
            let face = faceUp ? "face up" : "face down"
            throw CardGameError.IllegalMoveError(message: "Cannot add \(card) \(face) to pile \(self)")
        }
        cards.append(card)
        if !faceUp {
            indexOfTopMostFaceUpCard = cards.count
        }
    }
    
    func canAddCards(cards: [Card], faceUp: Bool) -> Bool {
        return validator.canAddCards(cards, faceUp: faceUp, toPile: self)
    }
    
    /// Adds the given cards to the top of the pile
    /// - Parameter cards: the cards to add
    /// - Parameter faceUp: whether the card should be added face up
    /// - Throws: `CardGameError.IllegalMoveError` if `!canAddCards(cards, faceUp: faceUp)`
    mutating func addCards(newCards: [Card], faceUp: Bool) throws {
        guard canAddCards(newCards, faceUp: faceUp) else {
            let face = faceUp ? "face up" : "face down"
            throw CardGameError.IllegalMoveError(message: "Cannot add \(newCards) \(face) to pile \(self)")
        }
        cards.appendContentsOf(newCards)
        if !faceUp {
            indexOfTopMostFaceUpCard = cards.count
        }
    }
    
    func canRevealTopCard() -> Bool {
        return !isEmpty && indexOfTopMostFaceUpCard == cards.count && validator.canRevealTopCardInPile(self)
    }
    
    mutating func revealTopCard() throws {
        guard canRevealTopCard() else {
            throw CardGameError.IllegalMoveError(message: "cannot reveal top card in pile \(self)")
        }
        indexOfTopMostFaceUpCard = cards.count - 1
    }
}

// CCC, 10/5/2015. Seems like we could abstract this and make a general AI or a general CLI
//protocol GameMove {
//    typealias GameState
//    
//    var localizedDescription: String { get }
//    var action: GameState -> GameState { get }
//}
//
//protocol Game {
//    typealias GameState
//    typealias GameMoveKind: GameMove
//    
//    var state: GameState { get }
//    func makeMove(move: GameMoveKind)
//    func legalMoves() -> [GameMoveKind]
//}

extension Suit: CustomStringConvertible {
    var description: String {
        switch self {
        case .Spades: return "♠️"
        case .Clubs: return "♣️"
        case .Diamonds: return "♦️"
        case .Hearts: return "♥️"
        }
    }
}

extension Rank: CustomStringConvertible {
    var description: String {
        switch self {
        case .Ace: return "A"
        case .Jack: return "J"
        case .Queen: return "Q"
        case .King: return "K"
        default:
            return String(rawValue)
        }
    }
}

extension Card: CustomStringConvertible {
    var description: String {
        return "\(rank)\(suit)"
    }
}

extension Pile: CustomStringConvertible {
    var description: String {
        if isEmpty {
            return "[ ]"
        }

        var result = ""
        if indexOfTopMostFaceUpCard != 0 {
            result += "[\(indexOfTopMostFaceUpCard) face down] "
        }
        for card in visibleCards {
            result += "\(card) "
        }
        return result.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
}



