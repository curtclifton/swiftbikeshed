//
//  Extensions.swift
//  Klondike
//
//  Created by Curt Clifton on 10/3/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

/// Returns a random number in the range lowerBound..<upperBound
func randRangeWithLowerBound(lowerBound: Int, upperBound: Int) -> Int {
    assert(lowerBound < upperBound)
    let realRandom = Double(arc4random()) / 4294967296.0
    let randInt = Int(floor(Double(upperBound - lowerBound) * realRandom))
    return randInt + lowerBound
}

extension CollectionType where Index.Distance == Int {
    // CCC, 10/5/2015.  should be a computed property
    func someValue() -> Generator.Element? {
        if count == 0 {
            return nil
        }
        
        let rangeSize = startIndex.distanceTo(endIndex)
        let offset = randRangeWithLowerBound(0, upperBound: rangeSize)
        let index = startIndex.advancedBy(offset)
        return self[index]
    }
}

extension CollectionType where Generator.Element: Hashable, Index.Distance == Int {
    var shuffled: [Generator.Element] {
        var set = Set(self)
        var result: [Generator.Element] = []
        while !set.isEmpty {
            guard let item = set.someValue() else { break }
            set.remove(item)
            result.append(item)
        }
        return result
    }
}
