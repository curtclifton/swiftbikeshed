//
//  KlondikeModel.swift
//  Klondike
//
//  Created by Curt Clifton on 10/3/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

struct FoundationValidator: PileOperationValidator {
    let suit: Suit
    
    func canAddCard(card: Card, faceUp: Bool, toPile pile: Pile) -> Bool {
        if card.suit != suit || !faceUp {
            return false
        }
        
        if pile.isEmpty {
            return card.rank == .Ace
        }
        
        guard let topCard = pile.visibleCards.last else {
            abort()
        }
        
        return card.rank == topCard.rank.nextHigherRank
    }
    
    func canAddCards(cards: [Card], faceUp: Bool, toPile pile: Pile) -> Bool {
        guard cards.count == 1 else {
            return false
        }
        return self.canAddCard(cards.first!, faceUp: faceUp, toPile: pile)
    }
    
    func canDrawCards(count: Int, fromPile: Pile) -> Bool {
        return false
    }
    
    func canRevealTopCardInPile(pile: Pile) -> Bool {
        return false
    }
}

struct TableauValidator: PileOperationValidator {
    static func doCardsAlternate(cards: [Card]) -> Bool {
        if cards.isEmpty {
            return true
        }
        var previousColor = cards.first!.suit.color
        var previousRank = cards.first!.rank
        for card in cards[1..<cards.count] {
            if card.suit.color == previousColor || card.rank.nextHigherRank != previousRank {
                return false
            }
            previousColor = card.suit.color
            previousRank = card.rank
        }
        return true
    }
    
    func canAddCards(cards: [Card], faceUp: Bool, toPile pile: Pile) -> Bool {
        guard let firstCard = cards.first else {
            return false
        }
        
        guard TableauValidator.doCardsAlternate(cards) else {
            return false
        }
        
        // Can only add stack beginning with King if pile is empty (stack must also alternate in color, but we checked that above)
        if firstCard.rank == .King {
            return pile.isEmpty
        }
        
        // if no visible cards, can't add anything (should have revealed a card when the stack was cleared)
        guard let topVisibleCard = pile.visibleCards.last else {
            return false
        }
        
        // otherwise can add if firstCard is opposite color of the top card in pile and if cards alternate colors and descend in rank
        return firstCard.suit.color != topVisibleCard.suit.color && firstCard.rank.nextHigherRank == topVisibleCard.rank
    }
    
    func canDrawCards(count: Int, fromPile: Pile) -> Bool {
        return count <= fromPile.visibleCards.count
    }
}

struct StockValidator: PileOperationValidator {
    func canAddUnknownCardFaceUp(faceUp: Bool) -> Bool {
        return false
    }
    
    func canAddCards(cards: [Card], faceUp: Bool, toPile pile: Pile) -> Bool {
        return pile.isEmpty && !faceUp
    }
    
    func canDrawCards(count: Int, fromPile pile: Pile) -> Bool {
        return count == 1
    }

    func canRevealTopCardInPile(pile: Pile) -> Bool {
        return false
    }
}

struct DiscardValidator: PileOperationValidator {
    func canAddCards(cards: [Card], faceUp: Bool, toPile pile: Pile) -> Bool {
        return cards.count == 1 && faceUp
    }
    
    func canDrawCards(count: Int, fromPile pile: Pile) -> Bool {
        return count == 1 || count == pile.count
    }

    func canRevealTopCardInPile(pile: Pile) -> Bool {
        return false
    }
}

class KlondikeGame {
    var state: KlondikeGameState
    
    init() {
        self.state = KlondikeGameState()
    }
    
    var isWon: Bool {
        return state.isWon
    }
    
    var isLost: Bool {
        return state.isLost
    }
    
    func legalMoves() -> [KlondikeMove] {
        return state.legalMoves()
    }
    
    func makeMove(move: KlondikeMove) {
        do {
            try state.makeMove(move)
        } catch {
            print("unexpected error thrown making move “\(move)”: \(error)")
            // CCC, 10/6/2015. mark game as lost and start returning empty legal moves?
        }
    }
}

struct KlondikeGameState {
    private(set) var foundations: [Pile]
    private(set) var stock: Pile
    private(set) var discardPile: Pile
    private(set) var tableau: [Pile]
    private(set) var hasResigned = false
    
    init() {
        self.foundations = Suit.allSuits().map { Pile(validator: FoundationValidator(suit: $0)) }
        self.stock = Pile(cards: Card.allCards().shuffled, faceUp: false, validator: StockValidator())
        self.discardPile = Pile(validator: DiscardValidator())

        // CCC, 10/7/2015. The stock is shuffled, so you could just deal to the tableau piles one pile at a time.
        var initialTableauCards = (1...7).map { _ in Array<Card>() }
        for numberOfPilesToDealTo in 7.stride(through: 1, by: -1) {
            for pileIndex in 0..<numberOfPilesToDealTo {
                try! initialTableauCards[pileIndex].append(stock.drawCard()) // stock must be sufficient for drawing from
            }
        }
        self.tableau = initialTableauCards.reverse().map { cards in
            var result = Pile(cards: cards, faceUp: false, validator: TableauValidator())
            try! result.revealTopCard() // TableauValidator must allow this
            return result
        }
    }
    
    func legalMoves() -> [KlondikeMove] {
        guard !hasResigned && !isWon && !isLost else { return [] }
        
        var result: [KlondikeMove] = []
        
        // If the Stock becomes empty, turn the entire discard pile over and make it the new Stock.
        if (!discardPile.isEmpty && discardPile.canDrawCards(discardPile.count) && stock.canAddCards(discardPile.cards, faceUp: false)) {
            result.append(.RecycleDiscard)
        }
        
        //Turn over the top card of the Stock and place it face-up on the Discard pile.
        if (stock.canDrawCards(1) && discardPile.canAddUnknownCardFaceUp(true)) {
            result.append(.DrawCard)
        }
        
        //Move a card from the discard pile to one of the foundation piles.
        if let topDiscardCard = discardPile.visibleCards.last where discardPile.canDrawCards(1) {
            for (index, foundation) in foundations.enumerate() {
                if foundation.canAddCard(topDiscardCard, faceUp: true) {
                    result.append(.MoveDiscardToFoundation(index: index))
                }
            }
        }

        //Move a card from the tableau to one of the foundation piles.
        for (tableauIndex, tableauPile) in tableau.enumerate() {
            if let topTableauCard = tableauPile.visibleCards.last where tableauPile.canDrawCards(1) {
                for (foundationIndex, foundation) in foundations.enumerate() {
                    if foundation.canAddCard(topTableauCard, faceUp: true) {
                        result.append(.MoveTableauToFoundation(sourceIndex: tableauIndex, destinationIndex: foundationIndex))
                    }
                }
            }
        }

        // Move the top card of the discard pile to one of the tableau piles. (Also handles moving a King.)
        if let topDiscardCard = discardPile.visibleCards.last where discardPile.canDrawCards(1) {
            for (tableauIndex, tableauPile) in tableau.enumerate() {
                if tableauPile.canAddCard(topDiscardCard, faceUp: true) {
                    result.append(.MoveDiscardToTableau(index: tableauIndex))
                }
            }
        }

        // Move one or more cards from one tableau pile to another. (Also handles moving a King.)
        for (sourceTableauIndex, sourceTableauPile) in tableau.enumerate() {
            let visibleSourceCards = sourceTableauPile.visibleCards
            for drawPosition in 0..<visibleSourceCards.count {
                let cardsToMove = Array(visibleSourceCards.suffixFrom(drawPosition))
                guard sourceTableauPile.canDrawCards(cardsToMove.count) else { continue }
                for (destinationTableauIndex, destinationTableauPile) in tableau.enumerate() {
                    guard sourceTableauIndex != destinationTableauIndex else { continue }
                    if destinationTableauPile.canAddCards(cardsToMove, faceUp: true) {
                        result.append(.MoveTableauToTableau(count: cardsToMove.count, sourceIndex: sourceTableauIndex, destinationIndex: destinationTableauIndex))
                    }
                }
            }
        }

        // Reveal top tableau card
        for (tableauIndex, tableauPile) in tableau.enumerate() {
            if tableauPile.canRevealTopCard() {
                result.append(.RevealTableauCard(index: tableauIndex))
            }
        }
        
        return result
    }
    
    mutating func makeMove(move: KlondikeMove) throws {
        switch move {
        case .DrawCard:
            let card = try stock.drawCard()
            try discardPile.addCard(card, faceUp: true)
        case .RecycleDiscard:
            let cards = try discardPile.drawCards(discardPile.count)
            try stock.addCards(cards, faceUp: false)
        case let .MoveDiscardToFoundation(index):
            let card = try discardPile.drawCard()
            try foundations[index].addCard(card, faceUp: true)
        case let .MoveDiscardToTableau(index):
            let card = try discardPile.drawCard()
            try tableau[index].addCard(card, faceUp: true)
        case let .MoveTableauToFoundation(sourceIndex, destinationIndex):
            let card = try tableau[sourceIndex].drawCard()
            try foundations[destinationIndex].addCard(card, faceUp: true)
        case let .MoveTableauToTableau(count, sourceIndex, destinationIndex):
            let cards = try tableau[sourceIndex].drawCards(count)
            try tableau[destinationIndex].addCards(cards, faceUp: true)
        case let .RevealTableauCard(index):
            try tableau[index].revealTopCard()
        case .Resign:
            hasResigned = true
        }
    }
    
    var isWon: Bool {
        return stock.isEmpty && discardPile.isEmpty && tableau.reduce(true, combine: { $0 && $1.isEmpty })
    }
    
    // CCC, 10/3/2015. implement loss detection
    var isLost: Bool {
        return hasResigned
    }
}

enum KlondikeMove {
    case Resign
    case DrawCard
    case RecycleDiscard
    case MoveDiscardToFoundation(index: Int)
    case MoveDiscardToTableau(index: Int)
    case MoveTableauToFoundation(sourceIndex: Int, destinationIndex: Int)
    case MoveTableauToTableau(count: Int, sourceIndex: Int, destinationIndex: Int)
    case RevealTableauCard(index: Int)
}

struct KlondikeMoveChooser {
    let resignationWearinessThreshold = 75
    let recklessnessWearinessThreshold = 50
    private(set) var wearinessLevel = 0
    private(set) var cyclingMoveCount = 0
    
    func weightForMove(move: KlondikeMove, gameState: KlondikeGameState) -> Int {
        let minimumFoundationRawValue: Int! = gameState.foundations.map( { (foundationPile: Pile) -> Int in
            if let topCard = foundationPile.visibleCards.last {
                return topCard.rank.rawValue
            } else {
                return 0
            }
        }).minElement(<)
        func shouldFavorFoundationMoveForCard(card: Card) -> Bool {
            if card.rank == .Ace || card.rank == .Two || wearinessLevel > recklessnessWearinessThreshold {
                return true
            }
            let result = card.rank.rawValue - 1 <= minimumFoundationRawValue
            return result
        }
        switch move {
        case .Resign: return 1
        case .DrawCard: return 1000
        case .RecycleDiscard: return 100
        case .MoveDiscardToFoundation(_):
            if let topDiscardCard = gameState.discardPile.visibleCards.last where shouldFavorFoundationMoveForCard(topDiscardCard) {
                return 100000
            }
            return 50
        case let .MoveTableauToFoundation(sourceIndex, _):
            if let card = gameState.tableau[sourceIndex].visibleCards.last where shouldFavorFoundationMoveForCard(card) {
                return 100000
            }
            return 50
        case .MoveDiscardToTableau(_): return 4000
        case let .MoveTableauToTableau(count, sourceIndex, _):
            if let highestMovingCard = gameState.tableau[sourceIndex].cards.first where count == gameState.tableau[sourceIndex].count && highestMovingCard.rank == .King {
                // don't bother moving a full stack rooted with a king
                return 1
            }
            return 400
        case .RevealTableauCard(_): return 100000
        }
    }
    
    private mutating func updateWearinessForMove(move: KlondikeMove) {
        switch move {
        case .RecycleDiscard:
            wearinessLevel++
        case .MoveDiscardToFoundation(_), .MoveTableauToFoundation(_):
            // any move to foundation is rejuvenating
            wearinessLevel = 0
        default:
            // CCC, 10/6/2015. Really only a cycling move if the face down counts in the tableau are unchanged
            cyclingMoveCount++
            if cyclingMoveCount > 75 {
                cyclingMoveCount = 0
                wearinessLevel++
            }
        }
    }
    
    mutating func chooseFromMoves(moves: [KlondikeMove], gameState: KlondikeGameState) -> KlondikeMove? {
        guard wearinessLevel < resignationWearinessThreshold else {
            // CCC, 10/7/2015. Might be interesting to check whether the burried cards form cycles with their blocking card.
            return .Resign
        }
        guard !moves.isEmpty else {
            return nil
        }
        var chosenMove: KlondikeMove?
        if moves.count == 1 {
            chosenMove = moves.first
        } else {
            let weights = moves.map { weightForMove($0, gameState: gameState) }
            let total = weights.reduce(0, combine: +)
            let randomPosition = randRangeWithLowerBound(0, upperBound: total)
            var consumedRandomPosition = randomPosition
            for (index, weight) in weights.enumerate() {
                if consumedRandomPosition < weight {
                    chosenMove = moves[index]
                    break
                }
                consumedRandomPosition -= weight
            }
            assert(chosenMove != nil, "Should have picked one of the moves: “\(moves)”, weights: \(weights), randomPosition: \(randomPosition)")
        }

        if let move = chosenMove {
            updateWearinessForMove(move)
        }
        return chosenMove
    }
    
    mutating func refresh() {
        wearinessLevel = 0
        cyclingMoveCount = 0
    }
}

extension KlondikeGame: CustomStringConvertible {
    var description: String {
        return "Foundation: \(state.foundations)\nTableau: \(state.tableau)\nStock: \(state.stock)\nDiscard: \(state.discardPile)"
    }
}

extension KlondikeMove: CustomStringConvertible {
    var description: String {
        switch self {
        case .Resign: return "resign"
        case .DrawCard: return "draw card"
        case .RecycleDiscard: return "recycle discard"
        case .MoveDiscardToFoundation(_): return "move discard to foundation"
        case .MoveDiscardToTableau(_): return "move discard to tableau"
        case .MoveTableauToFoundation(_): return "move tableau to foundation"
        case .MoveTableauToTableau(_): return "move between tableau piles"
        case .RevealTableauCard(_): return "reveal tableau card"
        }
    }
}
