//
//  ViewModel.swift
//  Klondike
//
//  Created by Curt Clifton on 10/9/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

struct ViewModel {
    var moveChooser = KlondikeMoveChooser()
    var klondikeGame = KlondikeGame()

    var moveCount = 0
    var gamesCompleted = 0
    var gamesWon = 0
    
    var runningStatsString: String {
        guard gamesCompleted != 0 else {
            return "0-0"
        }
        let percent = 100 * gamesWon / gamesCompleted
        return "Won \(gamesWon) of \(gamesCompleted) completed, \(percent)%"
    }
    
    var foundationTops: [Card?] {
        let result = klondikeGame.state.foundations.map { $0.cards.last }
        return result
    }
    
    var stockCount: Int {
        let result = klondikeGame.state.stock.count
        return result
    }
    
    var discardTop: Card? {
        let result = klondikeGame.state.discardPile.cards.last
        return result
    }
    
    var tableauBuriedCardCount: [Int] {
        let result = klondikeGame.state.tableau.map { pile in pile.count - pile.visibleCards.count }
        return result
    }
    
    var visibleTableauCards: [[Card]] {
        let result = klondikeGame.state.tableau.map { pile in pile.visibleCards }
        return result
    }

    mutating func startNewGame() {
        guard klondikeGame.isWon || klondikeGame.isLost else {
            print("quit game without a win or loss. wat?")
            abort()
        }
        if klondikeGame.isWon {
            gamesWon++
        }
        gamesCompleted++
        klondikeGame = KlondikeGame()
        moveChooser.refresh()
        moveCount = 0
        
        print(runningStatsString)
    }
    
    func logState(message: Any) {
        //        print(message)
    }
    
    mutating func makeMove() {
        guard !klondikeGame.isLost else {
            startNewGame()
            return
        }
        
        logState(klondikeGame)
        let legalMoves = klondikeGame.legalMoves()
        logState("legal moves: \(legalMoves)")
        logState("weariness: \(moveChooser.wearinessLevel)")
        guard let move = moveChooser.chooseFromMoves(legalMoves, gameState: klondikeGame.state) else {
            logState("out of moves")
            startNewGame()
            return
        }
        moveCount++
        logState("Won \(gamesWon) of \(gamesCompleted) completed: \(moveCount). make move: \(move)")
        klondikeGame.makeMove(move)
    }
}