//
//  GameScene.swift
//  Klondike
//
//  Created by Curt Clifton on 9/30/15.
//  Copyright (c) 2015 curtclifton.net. All rights reserved.
//

import SpriteKit

class KlondikeLabelNode: SKLabelNode {
    override init() {
        super.init()
        self.fontName = "Chalkduster"
        self.fontSize = 45
        self.text = ""
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

// CCC, 10/9/2015. Just drawing text at the moment, but could make it a sprite. Need to support face down/up
class CardNode: KlondikeLabelNode {
    var faceUp = true {
        didSet {
            if faceUp == oldValue { return }
            updateText()
        }
    }
    var showPlaceholder = false {
        didSet {
            if showPlaceholder == oldValue { return }
            updateText()
        }
    }
    var card: Card? {
        didSet {
            if card == oldValue { return }
            updateText()
        }
    }

    private func updateText() {
        if let actualCard = card {
            text = faceUp ? actualCard.description : "[?]"
        } else {
            text = showPlaceholder ? "[ ]" : ""
        }
    }
    
    override init() {
        super.init()
        updateText()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

class TableauPileNode: SKNode {
    private let buriedCardsNode = KlondikeLabelNode()
    private let visibleCardsNodes = (1...13).map { _ in CardNode() }
    
    var buriedCardCount = 0 {
        didSet {
            buriedCardsNode.text = "[\(buriedCardCount)]"
        }
    }
    
    var visibleCards: [Card] = [] {
        didSet {
            // set first visibleCards.count of the nodes to visibleCards, nil the rest
            for (index, node) in visibleCardsNodes.enumerate() {
                if index < visibleCards.count {
                    node.card = visibleCards[index]
                } else {
                    node.card = nil
                }
            }
        }
    }
    
    override init() {
        super.init()
        
        // CCC, 10/9/2015. Positioning is a hack
        let yStep = 42
        var cardY = yStep * (1 + visibleCardsNodes.count)
        buriedCardsNode.position = CGPoint(x: 0, y: cardY)
        self.addChild(buriedCardsNode)
        for visibleCardsNode in visibleCardsNodes {
            cardY -= yStep
            visibleCardsNode.position = CGPoint(x: 0, y: cardY)
            self.addChild(visibleCardsNode)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

class GameScene: SKScene {
    private var viewModelDidChange = false
    var viewModel = ViewModel() {
        didSet {
            viewModelDidChange = true
        }
    }
    let myLabel = KlondikeLabelNode()

    let foundations: [CardNode] = (1...4).map { _ in
        var foundation = CardNode()
        foundation.showPlaceholder = true
        return foundation
    }
    let stock = KlondikeLabelNode()
    let discard: CardNode = {
        var cardNode = CardNode()
        cardNode.showPlaceholder = true
        return cardNode
    }()
    let tableauPiles = (1...7).map { _ in TableauPileNode() }
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        myLabel.fontSize = 45
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMinY(self.frame) + 10)
        self.addChild(myLabel)
        
        // CCC, 10/9/2015. All this positioning is a hack.
        let foundationY = CGRectGetMaxY(self.frame) - 50
        var foundationX = CGRectGetMidX(self.frame)
        for foundation in foundations {
            foundation.position = CGPoint(x: foundationX, y: foundationY)
            foundationX += 100
            self.addChild(foundation)
        }
        
        stock.text = "[24]"
        stock.position = CGPoint(x: 100, y: 60)
        self.addChild(stock)
        
        discard.position = CGPoint(x: 200, y: 60)
        self.addChild(discard)
        
        var tableauX = 100
        let tableauY = 70
        for tableauPile in tableauPiles {
            tableauPile.position = CGPoint(x: tableauX, y: tableauY)
            tableauX += 120
            self.addChild(tableauPile)
        }
    }
    
    override func mouseDown(theEvent: NSEvent) {
        // CCC, 10/9/2015. If game over, start new game.
        /* Called when a mouse click occurs */
        
//        let location = theEvent.locationInNode(self)
//        
//        let sprite = SKSpriteNode(imageNamed:"Spaceship")
//        sprite.position = location;
//        sprite.setScale(0.5)
//        
//        let action = SKAction.rotateByAngle(CGFloat(M_PI), duration:1)
//        sprite.runAction(SKAction.repeatActionForever(action))
//        
//        self.addChild(sprite)
    }
    
    override func update(currentTime: CFTimeInterval) {
        viewModel.makeMove()
        if viewModelDidChange {
            viewModelDidChange = false

            // Update views
            myLabel.text = viewModel.runningStatsString
            for (index, card) in viewModel.foundationTops.enumerate() {
                foundations[index].card = card
            }
            stock.text = "[\(viewModel.stockCount)]"
            discard.card = viewModel.discardTop
            
            for (index, tableauPileNode) in tableauPiles.enumerate() {
                tableauPileNode.buriedCardCount = viewModel.tableauBuriedCardCount[index]
                tableauPileNode.visibleCards = viewModel.visibleTableauCards[index]
            }
        }
    }
}
