//: Playground - noun: a place where people can play

import Cocoa

struct Foo {
    var solved: Bool
}

let foos: [Foo?] = [Foo(solved: true), Foo(solved: false), nil]

for f in foos {
    if f?.solved ?? false  {
        print("yep: \(f)")
    } else {
        print("nope: \(f)")
    }
}


