//
//  TestExtensions.swift
//  Klondike
//
//  Created by Curt Clifton on 10/3/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation
import XCTest


extension XCTestCase {
    func expectThrow(caller: String = __FUNCTION__, block: () throws -> ()) {
        do {
            try block()
            XCTFail("expected test case “\(caller)” to throw but it did not")
        } catch {
            // yay!
            return
        }
    }

    func expectNoThrow(caller: String = __FUNCTION__, block: () throws -> ()) {
        do {
            try block()
        } catch {
            XCTFail("test case “\(caller)” unexpectedly threw “\(error)”")
        }
    }
}