//
//  CardGameModelTests.swift
//  Klondike
//
//  Created by Curt Clifton on 9/30/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import XCTest
@testable import Klondike

struct HeartsOnlyValidator: PileOperationValidator {
    func canAddCard(card: Card, faceUp: Bool, toPile: Pile) -> Bool {
        return card.suit == .Hearts
    }
}

class CardGameModelTests: XCTestCase {
    var emptyPile = Pile()
    var fullDeck = Pile()
    var heartsOnlyPile = Pile()

    override func setUp() {
        super.setUp()

        emptyPile = Pile()
        fullDeck = Pile(cards: Card.allCards(), faceUp: false)
        heartsOnlyPile = Pile(validator: HeartsOnlyValidator())
}
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testEmptyPile() {
        XCTAssertEqual(emptyPile.count, 0)
    }
    
    func testFullDeck() {
        XCTAssertEqual(fullDeck.count, 52)
    }

    func testShufflePile() {
        let cards = fullDeck.cards
        fullDeck.shuffle()
        // not really a correct test, as there is a vanishingly small chance of a random shuffle maintaining the order, ¯\_(ツ)_/¯
        XCTAssertNotEqual(fullDeck.cards, cards)
    }
    
    func testIsEmptyPile() {
        XCTAssert(emptyPile.isEmpty)
        XCTAssertFalse(fullDeck.isEmpty)
    }
    
    func testDrawCard() {
        expectThrow {
            try self.emptyPile.drawCard()
        }
        expectNoThrow {
            var card = try self.fullDeck.drawCard()
            XCTAssertEqual(card, Card(suit: .Hearts, rank: .King))
            card = try self.fullDeck.drawCard()
            XCTAssertEqual(card, Card(suit: .Hearts, rank: .Queen))
        }
    }
    
    func testCanAddCard() {
        XCTAssert(heartsOnlyPile.canAddCard(Card(suit: .Hearts, rank: .Ace), faceUp: true))
        XCTAssert(heartsOnlyPile.canAddCard(Card(suit: .Hearts, rank: .Three), faceUp: true))
        XCTAssertFalse(heartsOnlyPile.canAddCard(Card(suit: .Clubs, rank: .Seven), faceUp: true))
    }
    
    func testAddCard() {
        expectThrow {
            try self.heartsOnlyPile.addCard(Card(suit: .Diamonds, rank: .Jack), faceUp: true)
        }
        expectNoThrow {
            let cardsToAdd = [ Card(suit: .Hearts, rank: .Ace), Card(suit: .Hearts, rank: .Two) ]
            var pile = self.heartsOnlyPile
            for card in cardsToAdd {
                try pile.addCard(card, faceUp: true)
            }
            XCTAssertEqual(pile.cards, cardsToAdd)
        }
    }
    
    func testVisibleCards() {
        XCTAssert(emptyPile.visibleCards.isEmpty)

        let faceUpDeck = Pile(cards: fullDeck.cards, faceUp: true)
        XCTAssertEqual(faceUpDeck.visibleCards, fullDeck.cards)

        var faceDownDeck = Pile(cards: fullDeck.cards, faceUp: false)
        XCTAssert(faceDownDeck.visibleCards.isEmpty)

        expectNoThrow {
            // Add a couple more face down
            try faceDownDeck.addCard(Card(suit: .Hearts, rank: .Ace), faceUp: false)
            XCTAssert(faceDownDeck.visibleCards.isEmpty)
            try faceDownDeck.addCard(Card(suit: .Diamonds, rank: .Ace), faceUp: false)
            XCTAssert(faceDownDeck.visibleCards.isEmpty)

            // Then a couple face up
            try faceDownDeck.addCard(Card(suit: .Spades, rank: .Ace), faceUp: true)
            XCTAssertEqual(faceDownDeck.visibleCards, [Card(suit: .Spades, rank: .Ace)])
            try faceDownDeck.addCard(Card(suit: .Clubs, rank: .Ace), faceUp: true)
            XCTAssertEqual(faceDownDeck.visibleCards, [Card(suit: .Spades, rank: .Ace), Card(suit: .Clubs, rank: .Ace)])
        }
    }
    
    func testDrawCards() {
        var deck = fullDeck
        expectNoThrow {
            var drawnCards = try deck.drawCards(1)
            XCTAssertEqual(drawnCards, [Card(suit: .Hearts, rank: .King)])
            drawnCards = try deck.drawCards(2)
            XCTAssertEqual(drawnCards, [Card(suit: .Hearts, rank: .Jack), Card(suit: .Hearts, rank: .Queen)])
            try deck.drawCards(deck.count)
            XCTAssert(deck.isEmpty)
        }
        
        expectThrow {
            // deck is empty here, so can't draw
            try deck.drawCards(1)
        }
    }
    
    func testRevealTopCard() {
        expectThrow {
            XCTAssertFalse(self.emptyPile.canRevealTopCard())
            try self.emptyPile.revealTopCard()
        }
        
        var faceUpDeck = Pile(cards: fullDeck.cards, faceUp: true)
        expectThrow {
            XCTAssertFalse(faceUpDeck.canRevealTopCard())
            try faceUpDeck.revealTopCard()
        }
        
        var faceDownDeck = fullDeck
        expectNoThrow {
            XCTAssert(faceDownDeck.canRevealTopCard())
            try faceDownDeck.revealTopCard()
            XCTAssertEqual(faceDownDeck.visibleCards.count, 1)
        }
    }
    
    func testDescriptions() {
        XCTAssertEqual(emptyPile.description, "[ ]")
        XCTAssertEqual(fullDeck.description, "[52 face down]")
        let faceUpDeck = Pile(cards: fullDeck.cards, faceUp: true)
        XCTAssertEqual(faceUpDeck.description, "A♠️ 2♠️ 3♠️ 4♠️ 5♠️ 6♠️ 7♠️ 8♠️ 9♠️ 10♠️ J♠️ Q♠️ K♠️ A♣️ 2♣️ 3♣️ 4♣️ 5♣️ 6♣️ 7♣️ 8♣️ 9♣️ 10♣️ J♣️ Q♣️ K♣️ A♦️ 2♦️ 3♦️ 4♦️ 5♦️ 6♦️ 7♦️ 8♦️ 9♦️ 10♦️ J♦️ Q♦️ K♦️ A♥️ 2♥️ 3♥️ 4♥️ 5♥️ 6♥️ 7♥️ 8♥️ 9♥️ 10♥️ J♥️ Q♥️ K♥️")
        
        var mixedDeck = fullDeck
        expectNoThrow {
            try mixedDeck.addCard(Card(suit: .Clubs, rank: .Three), faceUp: true)
            XCTAssertEqual(mixedDeck.description, "[52 face down] 3♣️")
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}
