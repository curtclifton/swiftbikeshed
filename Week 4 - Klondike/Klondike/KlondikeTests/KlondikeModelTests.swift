//
//  KlondikeModelTests.swift
//  KlondikeTests
//
//  Created by Curt Clifton on 9/30/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import XCTest
@testable import Klondike

class KlondikeModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testKlondikeGameInit() {
        let game = KlondikeGame()
        // CCC, 10/3/2015. implement
        print(game)
    }
    
    func testFoundationValidator() {
        // CCC, 10/3/2015. implement
    }
    
    func testTableauValidator() {
        // CCC, 10/3/2015. implement
    }
    
    func testDiscardValidator() {
        // CCC, 10/3/2015. implement
    }
    
    func testStockValidator() {
        // CCC, 10/3/2015. implement
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
