# Answers to Bonus Questions

## Bonus Question 1

The implementation uses copy-on-write by virtue of being a struct backed by a single struct.

The basic set operations are O(1), mainly by virtue of the backing dictionary. `count` is maintained separately, otherwise it would be O(n) if we had to calculate it from the backing dictionary.

All of the binary operations on pairs of sets are O(n + m), where m is the number of elements in the other set.

## Bonus Question 2

I made CountedSet conform to CollectionType with an Index type of (Element, Int). So this is how you iterate over all the elements with counts:

    for (member, count) in myCountedSet { … }

If you want to just iterate over the members, I have a `members` var:

    for member in myCountedSet.members { … }

