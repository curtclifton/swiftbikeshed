//
//  OrderedOrderedCountedSet.swift
//  OrderedCountedSet
//
//  Created by Curt Clifton on 10/30/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

// -------------------------------------------------------------------------
// NOTE: This is pretty horrible. We're comparable, but only by keys.
struct Counter<Element: Comparable>: Comparable {
    let key: Element
    let count: Int
    
    static func forSearch(key: Element) -> Counter<Element> {
        return Counter(key: key, count: 0)
    }
}

func ==<Element: Comparable>(lhs: Counter<Element>, rhs: Counter<Element>) -> Bool {
    return lhs.key == rhs.key
}

func <<Element: Comparable>(lhs: Counter<Element>, rhs: Counter<Element>) -> Bool {
    return lhs.key < rhs.key
}
// -------------------------------------------------------------------------

// This is not like NSOrderedSet, which maintains insertion order, but rather is a sorted counted set of comparable elements.
struct OrderedCountedSet<Element: protocol<Hashable, Comparable>> {
    private var backingTree: RedBlackTree<Counter<Element>>
    private(set) var count: Int
    
    init() {
        self.backingTree = RedBlackTree<Counter<Element>>.empty()
        self.count = 0
    }
    
    init(array: [Element]) {
        self.init()
        for element in array {
            self.insert(element)
        }
    }
    
    private init(backingTree: RedBlackTree<Counter<Element>>) {
        self.backingTree = backingTree
        self.count = backingTree.reduce(0) { accum, counter in
            return accum + counter.count
        }
    }
    
    var isEmpty: Bool {
        return backingTree.isEmpty
    }
    
    // CCC, 11/4/2015. want this to be a LazyMapCollection once we implement CollectionType
    var members: LazyMapSequence<OrderedCountedSet<Element>, Element> {
        return LazyMapSequence(self) { $0.0 }
    }
    
    func contains(member: Element) -> Bool {
        return self[member] > 0
    }
    
    subscript(member: Element) -> Int {
        get {
            let searchTerm = Counter.forSearch(member)
            return backingTree.member(searchTerm)?.count ?? 0
        }
        set {
            let searchTerm = Counter(key: member, count: newValue)
            let currentCount = backingTree.member(searchTerm)?.count ?? 0
            count -= currentCount
            backingTree.insertOrReplace(searchTerm)
            count += newValue
        }
    }
    
    mutating func insert(member: Element) {
        let currentCount = self[member]
        let counter = Counter(key: member, count: currentCount + 1)
        backingTree.insertOrReplace(counter)
        count++
    }
    
    mutating func remove(member: Element) -> Element? {
        let currentCount = self[member]
        guard currentCount > 0 else { return nil }
        let counter = Counter(key: member, count: currentCount - 1)
        if currentCount == 0 {
            backingTree.remove(counter)
        } else {
            backingTree.insertOrReplace(counter)
        }
        count--
        return member
    }
    
    mutating func removeAll() {
        backingTree = RedBlackTree<Counter<Element>>.empty()
        count = 0
    }
    
    func isSubsetOfSet(other: OrderedCountedSet<Element>) -> Bool {
        for (element, count) in self {
            let otherCount = other[element]
            if count > otherCount { return false }
        }
        return true
    }
    
    func isStrictSubsetOfSet(other: OrderedCountedSet<Element>) -> Bool {
        return isSubsetOfSet(other) && !other.isSubsetOfSet(self)
    }
    
    func isSupersetOfSet(other: OrderedCountedSet<Element>) -> Bool {
        return other.isSubsetOfSet(self)
    }
    
    func isStrictSupersetOfSet(other: OrderedCountedSet<Element>) -> Bool {
        return other.isStrictSubsetOfSet(self)
    }
    
    func isDisjointWithSet(other: OrderedCountedSet<Element>) -> Bool {
        return self.intersectWithSet(other).isEmpty
    }
    
    func unionWithSet(other: OrderedCountedSet<Element>) -> OrderedCountedSet<Element> {
        let newBackingTree = newBackingTreeWithSet(other, operation: +)
        let result = OrderedCountedSet(backingTree: newBackingTree)
        return result
    }
    
    mutating func unionInPlaceWithSet(other: OrderedCountedSet<Element>) {
        self = unionWithSet(other)
    }
    
    func subtractSet(other: OrderedCountedSet<Element>) -> OrderedCountedSet<Element> {
        let newBackingTree = newBackingTreeWithSet(other, operation: -)
        let result = OrderedCountedSet(backingTree: newBackingTree)
        return result
    }
    
    mutating func subtractInPlaceWithSet(other: OrderedCountedSet<Element>) {
        self = subtractSet(other)
    }
    
    func intersectWithSet(other: OrderedCountedSet<Element>) -> OrderedCountedSet<Element> {
        let newBackingTree = newBackingTreeWithSet(other, operation: min)
        let result = OrderedCountedSet(backingTree: newBackingTree)
        return result
    }
    
    mutating func intersectInPlaceWithSet(other: OrderedCountedSet<Element>) {
        self = intersectWithSet(other)
    }
    
    func exclusiveOrWithSet(other: OrderedCountedSet<Element>) -> OrderedCountedSet<Element> {
        let newBackingTree = newBackingTreeWithSet(other, operation: OrderedCountedSet.exclusiveOrOp)
        let result = OrderedCountedSet(backingTree: newBackingTree)
        return result
    }
    
    mutating func exclusiveOrInPlaceWithSet(other: OrderedCountedSet<Element>) {
        self = exclusiveOrWithSet(other)
    }
    
    private static func exclusiveOrOp(count: Int, _ otherCount: Int) -> Int {
        return abs(count - otherCount)
    }
    
    private func newBackingTreeWithSet(other: OrderedCountedSet<Element>, operation: (count: Int, otherCount: Int) -> Int) -> RedBlackTree<Counter<Element>> {
        var resultCounters: [Counter<Element>] = []
        
        // Merge from generators of each set
        let generator = generate()
        let otherGenerator = other.generate()
        var nextCounter = generator.next()
        var nextOtherCounter = otherGenerator.next()
        while nextCounter != nil || nextOtherCounter != nil {
            let count: Int
            let otherCount: Int
            let element: Element
            switch (nextCounter, nextOtherCounter) {
            case (.None, .None):
                abort() // loop condition should make this unreachable
            case let (.None, .Some(other)):
                count = 0
                otherCount = other.1
                element = other.0
                nextOtherCounter = otherGenerator.next()
            case let (.Some(this), .None):
                count = this.1
                otherCount = 0
                element = this.0
                nextCounter = generator.next()
            case let (.Some(this), .Some(other)):
                if this.0 == other.0 {
                    count = this.1
                    otherCount = other.1
                    element = this.0
                    nextCounter = generator.next()
                    nextOtherCounter = otherGenerator.next()
                } else if this.0 < other.0 {
                    count = this.1
                    otherCount = 0
                    element = this.0
                    nextCounter = generator.next()
                } else {
                    count = 0
                    otherCount = other.1
                    element = other.0
                    nextOtherCounter = otherGenerator.next()
                }
            }
            let resultCount = operation(count: count, otherCount: otherCount)
            if resultCount > 0 {
                let resultCounter = Counter(key: element, count: resultCount)
                resultCounters.append(resultCounter)
            }
        }
        

        let result: RedBlackTree<Counter<Element>> = RedBlackTree<Counter<Element>>.fromSortedArray(resultCounters)
        return result
    }
}

// CCC, 11/4/2015. Want to extend to be a CollectionType
extension OrderedCountedSet: SequenceType {
    typealias OrderedCountedSetGenerator = AnyGenerator<(Element, Int)>
//    typealias OrderedCountedSetIndex = Int
    
//    var startIndex: OrderedCountedSetIndex {
//        return backingTree.values.startIndex
//    }
//    
//    var endIndex: OrderedCountedSetIndex {
//        return backingTree.values.endIndex
//    }
//    
    func generate() -> OrderedCountedSetGenerator {
        let lazySequence = backingTree.lazy.map { counter in
            return (counter.key, counter.count)
        }
        return AnySequence(lazySequence).generate()
    }
    
//    subscript(position: OrderedCountedSetIndex) -> (Element, Int) {
//        let counter = backingTree.values[position]
//        return (counter.key, counter.count)
//    }
}

extension OrderedCountedSet: Hashable {
    var hashValue: Int {
        let result = backingTree.reduce(0) { accum, counter in
            return (accum << 5) ^ ((counter.key.hashValue << 3) + counter.count)
        }
        return result
    }
}

func ==<T: Hashable>(lhs: OrderedCountedSet<T>, rhs: OrderedCountedSet<T>) -> Bool {
    return lhs.isSubsetOfSet(rhs) && rhs.isSubsetOfSet(lhs)
}
