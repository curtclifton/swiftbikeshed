//
//  CountedSet.swift
//  CountedSet
//
//  Created by Curt Clifton on 10/30/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import Foundation

struct CountedSet<Element: Hashable> {
    private var backingDictionary: [Element: Int]
    private(set) var count: Int
    
    init() {
        self.backingDictionary = [:]
        self.count = 0
    }
    
    init(array: [Element]) {
        self.init()
        for element in array {
            self.insert(element)
        }
    }
    
    private init(backingDictionary: [Element: Int]) {
        self.backingDictionary = backingDictionary
        self.count = backingDictionary.values.reduce(0, combine: +)
    }
    
    var isEmpty: Bool {
        return backingDictionary.isEmpty
    }
    
    var members: LazyMapCollection<CountedSet<Element>, Element> {
        return LazyMapCollection(self) { keyCountPair in keyCountPair.0 }
    }

    func contains(member: Element) -> Bool {
        return self[member] > 0
    }

    subscript(member: Element) -> Int {
        get {
            return backingDictionary[member] ?? 0
        }
        set {
            count -= (backingDictionary[member] ?? 0)
            backingDictionary[member] = newValue
            count += newValue
        }
    }
    
    mutating func insert(member: Element) {
        let currentCount = self[member]
        backingDictionary[member] = currentCount + 1
        count++
    }

    mutating func remove(member: Element) -> Element? {
        let currentCount = self[member]
        guard currentCount > 0 else { return nil }
        if currentCount == 0 {
            backingDictionary[member] = nil
        } else {
            backingDictionary[member] = currentCount - 1
        }
        count--
        return member
    }
    
    mutating func removeAll() {
        backingDictionary = [:]
        count = 0
    }

    func isSubsetOfSet(other: CountedSet<Element>) -> Bool {
        for (element, count) in backingDictionary {
            let otherCount = other[element]
            if count > otherCount { return false }
        }
        return true
    }
    
    func isStrictSubsetOfSet(other: CountedSet<Element>) -> Bool {
        return isSubsetOfSet(other) && !other.isSubsetOfSet(self)
    }

    func isSupersetOfSet(other: CountedSet<Element>) -> Bool {
        return other.isSubsetOfSet(self)
    }

    func isStrictSupersetOfSet(other: CountedSet<Element>) -> Bool {
        return other.isStrictSubsetOfSet(self)
    }
    
    func isDisjointWithSet(other: CountedSet<Element>) -> Bool {
        return intersectWithSet(other).isEmpty
    }
    
    func unionWithSet(other: CountedSet<Element>) -> CountedSet<Element> {
        let newBackingDictionary = newBackingDictionaryWithSet(other, operation: +)
        let result = CountedSet(backingDictionary: newBackingDictionary)
        return result
    }
    
    mutating func unionInPlaceWithSet(other: CountedSet<Element>) {
        self = unionWithSet(other)
    }
    
    func subtractSet(other: CountedSet<Element>) -> CountedSet<Element> {
        let newBackingDictionary = newBackingDictionaryWithSet(other, operation: -)
        let result = CountedSet(backingDictionary: newBackingDictionary)
        return result
    }
    
    mutating func subtractInPlaceWithSet(other: CountedSet<Element>) {
        self = subtractSet(other)
    }
    
    func intersectWithSet(other: CountedSet<Element>) -> CountedSet<Element> {
        let newBackingDictionary = newBackingDictionaryWithSet(other, operation: min)
        let result = CountedSet(backingDictionary: newBackingDictionary)
        return result
    }

    mutating func intersectInPlaceWithSet(other: CountedSet<Element>) {
        self = intersectWithSet(other)
    }
    
    func exclusiveOrWithSet(other: CountedSet<Element>) -> CountedSet<Element> {
        let newBackingDictionary = newBackingDictionaryWithSet(other, operation: CountedSet.exclusiveOrOp)
        let result = CountedSet(backingDictionary: newBackingDictionary)
        return result
    }
    
    mutating func exclusiveOrInPlaceWithSet(other: CountedSet<Element>) {
        self = exclusiveOrWithSet(other)
    }
    
    private static func exclusiveOrOp(count: Int, _ otherCount: Int) -> Int {
        return abs(count - otherCount)
    }
    
    private func newBackingDictionaryWithSet(other: CountedSet<Element>, operation: (count: Int, otherCount: Int) -> Int) -> [Element: Int] {
        var result: [Element: Int] = [:]
        var members = Set(backingDictionary.keys)
        members.unionInPlace(other.backingDictionary.keys)
        for member in members {
            let count = self[member]
            let otherCount = other[member]
            let commonCount = operation(count: count, otherCount: otherCount)
            if commonCount > 0 {
                result[member] = commonCount
            }
        }
        return result
    }
}

extension CountedSet: CollectionType {
    typealias CountedSetGenerator = DictionaryGenerator<Element, Int>
    typealias CountedSetIndex = DictionaryIndex<Element, Int>
    
    var startIndex: CountedSetIndex {
        return backingDictionary.startIndex
    }
    
    var endIndex: CountedSetIndex {
        return backingDictionary.endIndex
    }
    
    func generate() -> CountedSetGenerator {
        return backingDictionary.generate()
    }
    
    subscript(position: CountedSetIndex) -> (Element, Int) {
        return backingDictionary[position]
    }
}

extension CountedSet: Hashable {
    var hashValue: Int {
        let result = backingDictionary.reduce(0) { accum, keyValuePair in
            return (accum << 5) ^ ((keyValuePair.0.hashValue << 3) + keyValuePair.1)
        }
        return result
    }
}

func ==<T: Hashable>(lhs: CountedSet<T>, rhs: CountedSet<T>) -> Bool {
    return lhs.isSubsetOfSet(rhs) && rhs.isSubsetOfSet(lhs)
}