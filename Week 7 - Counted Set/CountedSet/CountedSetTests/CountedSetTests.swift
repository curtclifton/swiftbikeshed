//
//  CountedSetTests.swift
//  CountedSetTests
//
//  Created by Curt Clifton on 10/30/15.
//  Copyright © 2015 curtclifton.net. All rights reserved.
//

import XCTest
@testable import CountedSet

class CountedSetTests: XCTestCase {
    private func countedSetFromString(string: String) -> CountedSet<String> {
        let substrings = string.characters.map { String($0) }
        let result = CountedSet(array: substrings)
        return result
    }
    
    var emptySet: CountedSet<String>! = nil
    var singletonSet: CountedSet<String>! = nil
    var fibSet: CountedSet<String>! = nil
    
    override func setUp() {
        super.setUp()
        emptySet = CountedSet()
        singletonSet = countedSetFromString("B")
        fibSet = countedSetFromString("BCDDEEEFFFFFGGGGGGGG")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCount() {
        XCTAssertEqual(emptySet.count, 0)
        XCTAssertEqual(singletonSet.count, 1)
        XCTAssertEqual(fibSet.count, 20)
    }
    
    func testIsEmpty() {
        XCTAssert(emptySet.isEmpty)
        XCTAssertFalse(singletonSet.isEmpty)
        XCTAssertFalse(fibSet.isEmpty)
    }
    
    func testMembers() {
        func accumMembers(var accum: Set<String>, member: String) -> Set<String> {
            accum.insert(member)
            return accum
        }
        var members: Set<String> = emptySet.members.reduce(Set(), combine: accumMembers)
        XCTAssertEqual(members, [])
        members = singletonSet.members.reduce(Set(), combine: accumMembers)
        XCTAssertEqual(members, ["B"])
        members = fibSet.members.reduce(Set(), combine: accumMembers)
        XCTAssertEqual(members, ["B","C","D","E","F","G"])
    }
    
    func testContains() {
        func containsTesterWithString(string: String, expected: (Bool, Bool, Bool)) {
            XCTAssertEqual(emptySet.contains(string), expected.0)
            XCTAssertEqual(singletonSet.contains(string), expected.1)
            XCTAssertEqual(fibSet.contains(string), expected.2)
        }
        
        containsTesterWithString("A", expected: (false, false, false))
        containsTesterWithString("B", expected: (false, true, true))
        containsTesterWithString("C", expected: (false, false, true))
        containsTesterWithString("D", expected: (false, false, true))
        containsTesterWithString("E", expected: (false, false, true))
        containsTesterWithString("F", expected: (false, false, true))
        containsTesterWithString("G", expected: (false, false, true))
    }
    
    func testCountOf() {
        func countTesterWithString(string: String, expected: (Int, Int, Int)) {
            XCTAssertEqual(emptySet[string], expected.0)
            XCTAssertEqual(singletonSet[string], expected.1)
            XCTAssertEqual(fibSet[string], expected.2)
        }
        
        countTesterWithString("A", expected: (0, 0, 0))
        countTesterWithString("B", expected: (0, 1, 1))
        countTesterWithString("C", expected: (0, 0, 1))
        countTesterWithString("D", expected: (0, 0, 2))
        countTesterWithString("E", expected: (0, 0, 3))
        countTesterWithString("F", expected: (0, 0, 5))
        countTesterWithString("G", expected: (0, 0, 8))
    }
    
    func testInsert() {
        // The helper function to create test sets routes through insert, so it's already mostly tested.
        var set = singletonSet
        set.insert("B")
        XCTAssertEqual(set.count, 2)
        XCTAssertEqual(set["B"], 2)
        XCTAssertEqual(set["C"], 0)
        set.insert("C")
        XCTAssertEqual(set.count, 3)
        XCTAssertEqual(set["B"], 2)
        XCTAssertEqual(set["C"], 1)
    }
    
    func testRemove() {
        var set = fibSet
        XCTAssertEqual(set.count, 20)
        XCTAssertEqual(set["D"], 2)
        set.remove("D")
        XCTAssertEqual(set.count, 19)
        XCTAssertEqual(set["D"], 1)
        set.remove("D")
        XCTAssertEqual(set.count, 18)
        XCTAssertEqual(set["D"], 0)
        set.remove("D")
        XCTAssertEqual(set.count, 18)
        XCTAssertEqual(set["D"], 0)
    }
    
    func testRemoveAll() {
        var set = fibSet
        XCTAssertEqual(set.count, 20)
        XCTAssertEqual(set["D"], 2)
        set.removeAll()
        XCTAssertEqual(set.count, 0)
        XCTAssertEqual(set["D"], 0)
    }
    
    func testSubset() {
        XCTAssert(emptySet.isSubsetOfSet(emptySet))
        XCTAssert(emptySet.isSubsetOfSet(singletonSet))
        XCTAssert(emptySet.isSubsetOfSet(fibSet))

        XCTAssertFalse(singletonSet.isSubsetOfSet(emptySet))
        XCTAssertFalse(fibSet.isSubsetOfSet(emptySet))
        
        XCTAssert(singletonSet.isSubsetOfSet(singletonSet))
        XCTAssert(singletonSet.isSubsetOfSet(fibSet))
        XCTAssertFalse(fibSet.isSubsetOfSet(singletonSet))
    }
    
    func testStrictSubset() {
        XCTAssertFalse(emptySet.isStrictSubsetOfSet(emptySet))
        XCTAssert(emptySet.isStrictSubsetOfSet(singletonSet))
        XCTAssert(emptySet.isStrictSubsetOfSet(fibSet))
        
        XCTAssertFalse(singletonSet.isStrictSubsetOfSet(emptySet))
        XCTAssertFalse(fibSet.isStrictSubsetOfSet(emptySet))
        
        XCTAssertFalse(singletonSet.isStrictSubsetOfSet(singletonSet))
        XCTAssert(singletonSet.isStrictSubsetOfSet(fibSet))
        XCTAssertFalse(fibSet.isStrictSubsetOfSet(singletonSet))
    }
    
    func testSuperset() {
        XCTAssert(emptySet.isSupersetOfSet(emptySet))
        XCTAssertFalse(emptySet.isSupersetOfSet(singletonSet))
        XCTAssertFalse(emptySet.isSupersetOfSet(fibSet))
        
        XCTAssert(singletonSet.isSupersetOfSet(emptySet))
        XCTAssert(fibSet.isSupersetOfSet(emptySet))
        
        XCTAssert(singletonSet.isSupersetOfSet(singletonSet))
        XCTAssertFalse(singletonSet.isSupersetOfSet(fibSet))
        XCTAssert(fibSet.isSupersetOfSet(singletonSet))
    }
    
    func testStrictSuperset() {
        XCTAssertFalse(emptySet.isStrictSupersetOfSet(emptySet))
        XCTAssertFalse(emptySet.isStrictSupersetOfSet(singletonSet))
        XCTAssertFalse(emptySet.isStrictSupersetOfSet(fibSet))
        
        XCTAssert(singletonSet.isStrictSupersetOfSet(emptySet))
        XCTAssert(fibSet.isStrictSupersetOfSet(emptySet))
        
        XCTAssertFalse(singletonSet.isStrictSupersetOfSet(singletonSet))
        XCTAssertFalse(singletonSet.isStrictSupersetOfSet(fibSet))
        XCTAssert(fibSet.isStrictSupersetOfSet(singletonSet))
    }
    
    func testDisjoint() {
        XCTAssert(emptySet.isDisjointWithSet(emptySet))
        XCTAssert(emptySet.isDisjointWithSet(singletonSet))
        XCTAssert(emptySet.isDisjointWithSet(fibSet))
        
        XCTAssert(singletonSet.isDisjointWithSet(emptySet))
        XCTAssertFalse(singletonSet.isDisjointWithSet(singletonSet))
        XCTAssertFalse(singletonSet.isDisjointWithSet(fibSet))
        
        XCTAssert(fibSet.isDisjointWithSet(emptySet))
        XCTAssertFalse(fibSet.isDisjointWithSet(singletonSet))
        XCTAssertFalse(fibSet.isDisjointWithSet(fibSet))
    }
    
    func testUnion() {
        XCTAssertEqual(emptySet.unionWithSet(emptySet), emptySet)
        XCTAssertEqual(emptySet.unionWithSet(singletonSet), singletonSet)
        XCTAssertEqual(emptySet.unionWithSet(fibSet), fibSet)
        
        var s2Set = singletonSet
        s2Set.insert("B")
        var sfSet = fibSet
        sfSet.insert("B")

        XCTAssertEqual(singletonSet.unionWithSet(emptySet), singletonSet)
        XCTAssertEqual(singletonSet.unionWithSet(singletonSet), s2Set)
        XCTAssertEqual(singletonSet.unionWithSet(fibSet), sfSet)
        
        XCTAssertEqual(fibSet.unionWithSet(emptySet), fibSet)
        XCTAssertEqual(fibSet.unionWithSet(singletonSet), sfSet)
        
        let fib2Set = fibSet.unionWithSet(fibSet)
        for member in fibSet.members {
            XCTAssertEqual(fibSet[member] * 2, fib2Set[member])
        }
    }
    
    func testUnionInPlace() {
        var sfSet = fibSet
        sfSet.insert("B")
        
        var set = fibSet
        set.unionInPlaceWithSet(emptySet)
        XCTAssertEqual(set, fibSet)
        
        set.unionInPlaceWithSet(singletonSet)
        XCTAssertEqual(set, sfSet)
        
        var fib2Set = fibSet
        fib2Set.unionInPlaceWithSet(fibSet)
        for member in fibSet.members {
            XCTAssertEqual(fibSet[member] * 2, fib2Set[member])
        }
    }
    
    func testSubtract() {
        XCTAssertEqual(emptySet.subtractSet(emptySet), emptySet)
        XCTAssertEqual(emptySet.subtractSet(singletonSet), emptySet)
        XCTAssertEqual(emptySet.subtractSet(fibSet), emptySet)
        
        var sfSet = fibSet
        sfSet.remove("B")
        
        XCTAssertEqual(singletonSet.subtractSet(emptySet), singletonSet)
        XCTAssertEqual(singletonSet.subtractSet(singletonSet), emptySet)
        XCTAssertEqual(singletonSet.subtractSet(fibSet), emptySet)
        
        XCTAssertEqual(fibSet.subtractSet(emptySet), fibSet)
        XCTAssertEqual(fibSet.subtractSet(singletonSet), sfSet)
        XCTAssertEqual(fibSet.subtractSet(fibSet), emptySet)
    }
    
    func testIntersect() {
        XCTAssertEqual(emptySet.intersectWithSet(emptySet), emptySet)
        XCTAssertEqual(emptySet.intersectWithSet(singletonSet), emptySet)
        XCTAssertEqual(emptySet.intersectWithSet(fibSet), emptySet)
        
        XCTAssertEqual(singletonSet.intersectWithSet(emptySet), emptySet)
        XCTAssertEqual(singletonSet.intersectWithSet(singletonSet), singletonSet)
        XCTAssertEqual(singletonSet.intersectWithSet(fibSet), singletonSet)
        
        XCTAssertEqual(fibSet.intersectWithSet(emptySet), emptySet)
        XCTAssertEqual(fibSet.intersectWithSet(singletonSet), singletonSet)
        XCTAssertEqual(fibSet.intersectWithSet(fibSet), fibSet)
    }
    
    func testExclusiveOr() {
        XCTAssertEqual(emptySet.exclusiveOrWithSet(emptySet), emptySet)
        XCTAssertEqual(emptySet.exclusiveOrWithSet(singletonSet), singletonSet)
        XCTAssertEqual(emptySet.exclusiveOrWithSet(fibSet), fibSet)
        
        var sfSet = fibSet
        sfSet.remove("B")
        
        XCTAssertEqual(singletonSet.exclusiveOrWithSet(emptySet), singletonSet)
        XCTAssertEqual(singletonSet.exclusiveOrWithSet(singletonSet), emptySet)
        XCTAssertEqual(singletonSet.exclusiveOrWithSet(fibSet), sfSet)
        
        XCTAssertEqual(fibSet.exclusiveOrWithSet(emptySet), fibSet)
        XCTAssertEqual(fibSet.exclusiveOrWithSet(singletonSet), sfSet)
        XCTAssertEqual(fibSet.exclusiveOrWithSet(fibSet), emptySet)
    }

    func testGenerate() {
        for (element, count) in fibSet {
            switch element {
            case "B":
                XCTAssertEqual(count, 1)
            case "C":
                XCTAssertEqual(count, 1)
            case "D":
                XCTAssertEqual(count, 2)
            case "E":
                XCTAssertEqual(count, 3)
            case "F":
                XCTAssertEqual(count, 5)
            case "G":
                XCTAssertEqual(count, 8)
            default:
                XCTFail("Unexpected element “\(element)”")
            }
        }
    }
    
    func testAMillionUnions() {
        self.measureBlock {
            var o1 = CountedSet<String>()
            o1.insert("A")
            o1.insert("A")
            o1.insert("B")
            o1.insert("C")
            o1.insert("D")
            var o2 = o1
            o2.remove("B")
            
            var sets: [CountedSet<String>] = []
            for _ in 1...1000000 {
                sets.append(o1)
                o1 = o1.unionWithSet(o2)
            }
        }
    }
    
    func testAMillionInserts() {
        self.measureBlock {
            var o = CountedSet<Int>()
            
            for i in 1...1000000 {
                o.insert(i)
            }
        }
    }
    
    func testAMillionReverseOrderedInserts() {
        self.measureBlock {
            var o = CountedSet<Int>()
            
            for i in 1...1000000 {
                o.insert(1000000-i)
            }
        }
    }
}
