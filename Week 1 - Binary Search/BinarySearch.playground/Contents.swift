//: Swift Bike Shed, Week 1, http://codekata.com/kata/kata02-karate-chop/

import Cocoa

/// A simple test function so we can use Dave's test cases.
func assert_equal(expected: Int, _ actual: Int) -> Bool {
        return (expected == actual)
}

func assert_equal<T: Comparable>(expected: T?, _ actual: T?) -> Bool {
    return (expected == actual)
}

// CCC, 9/15/2015. Can we guarantee a sorted sequence using the type system? Probably not without dependent types.

func recursiveNonGenericChop(searchTerm: Int, _ array: [Int]) -> Int {
    func helper(slice: ArraySlice<Int>) -> Int {
        switch slice.count {
        case 0:
            return -1
        case 1:
            if slice[slice.startIndex] == searchTerm {
                return slice.startIndex
            } else {
                return -1
            }
        default:
            let middleIndex = (slice.startIndex + slice.endIndex) / 2
            let middleValue = slice[middleIndex]
            if searchTerm < middleValue {
                return helper(slice[slice.startIndex..<middleIndex])
            } else {
                return helper(slice[middleIndex..<slice.endIndex])
            }
        }
    }
    return helper(array[0..<array.count])
}

func iterativeNonGenericChop(searchTerm: Int, _ array: [Int]) -> Int {
    var slice = array[0..<array.count]
    while slice.count > 1 {
        let middleIndex = (slice.startIndex + slice.endIndex) / 2
        let middleValue = slice[middleIndex]
        if searchTerm < middleValue {
            slice = slice[slice.startIndex..<middleIndex]
        } else {
            slice = slice[middleIndex..<slice.endIndex]
        }
    }
    
    if slice.count == 0 {
        return -1
    } else {
        assert(slice.count == 1)
        if slice[slice.startIndex] == searchTerm {
            return slice.startIndex
        } else {
            return -1
        }
    }
}

func recursiveGenericChop<T: Comparable>(searchTerm: T, _ array: [T]) -> Int {
    func helper(slice: ArraySlice<T>) -> Int {
        switch slice.count {
        case 0:
            return -1
        case 1:
            if slice[slice.startIndex] == searchTerm {
                return slice.startIndex
            } else {
                return -1
            }
        default:
            let middleIndex = (slice.startIndex + slice.endIndex) / 2
            let middleValue = slice[middleIndex]
            if searchTerm < middleValue {
                return helper(slice[slice.startIndex..<middleIndex])
            } else {
                return helper(slice[middleIndex..<slice.endIndex])
            }
        }
    }
    return helper(array[0..<array.count])
}

/// I haven't been able to figure out how to find the middle index with a generic index type. ¯\_(ツ)_/¯
/*
func recursiveMoreGenericChop<T: Comparable, S: CollectionType, IndexType: IntegerArithmeticType where S.Generator.Element == T, S.Index == IndexType, S.SubSequence == S>(searchTerm: T, _ sortedCollection: S) -> IndexType? {
    func helper(subsequence: S.SubSequence) -> IndexType? {
        switch subsequence.count {
        case 0:
            return nil
        case 1:
            if subsequence[subsequence.startIndex] == searchTerm {
                return subsequence.startIndex
            } else {
                return nil
            }
        default:
            let startIndex = subsequence.startIndex
            let endIndex = subsequence.endIndex
            let middleIndex = (startIndex + endIndex) / 2
            let middleValue = subsequence[middleIndex]
            if searchTerm < middleValue {
                return helper(subsequence[subsequence.startIndex..<middleIndex])
            } else {
                return helper(subsequence[middleIndex..<subsequence.endIndex])
            }
        }
    }
    return helper(sortedCollection.suffixFrom(sortedCollection.startIndex))
}
*/

// Forcing the index type to be Int, I can get this function to compile, but I'm unable to come up with the magic incantation to actually invoke it.
func recursiveMoreGenericChop<T: Comparable, S: CollectionType where S.Generator.Element == T, S.Index == Int, S.SubSequence == S>(searchTerm: T, _ sortedCollection: S) -> Int {
    func helper(subsequence: S.SubSequence) -> Int {
        switch subsequence.count {
        case 0:
            return -1
        case 1:
            if subsequence[subsequence.startIndex] == searchTerm {
                return subsequence.startIndex
            } else {
                return -1
            }
        default:
            let startIndex = subsequence.startIndex
            let endIndex = subsequence.endIndex
            let middleIndex = (startIndex + endIndex) / 2
            let middleValue = subsequence[middleIndex]
            if searchTerm < middleValue {
                return helper(subsequence[subsequence.startIndex..<middleIndex])
            } else {
                return helper(subsequence[middleIndex..<subsequence.endIndex])
            }
        }
    }
    return helper(sortedCollection.suffixFrom(sortedCollection.startIndex))
}

//recursiveMoreGenericChop(3, [1,3,5])

//let chop = recursiveNonGenericChop
//let chop = iterativeNonGenericChop
let chop: (Int, [Int]) -> Int = recursiveGenericChop // type inference couldn't handle this one

// Test cases from the kata
assert_equal(-1, chop(3, []))
assert_equal(-1, chop(3, [1]))
assert_equal(0,  chop(1, [1]))

assert_equal(0,  chop(1, [1, 3, 5]))
assert_equal(1,  chop(3, [1, 3, 5]))
assert_equal(2,  chop(5, [1, 3, 5]))
assert_equal(-1, chop(0, [1, 3, 5]))
assert_equal(-1, chop(2, [1, 3, 5]))
assert_equal(-1, chop(4, [1, 3, 5]))
assert_equal(-1, chop(6, [1, 3, 5]))

assert_equal(0,  chop(1, [1, 3, 5, 7]))
assert_equal(1,  chop(3, [1, 3, 5, 7]))
assert_equal(2,  chop(5, [1, 3, 5, 7]))
assert_equal(3,  chop(7, [1, 3, 5, 7]))
assert_equal(-1, chop(0, [1, 3, 5, 7]))
assert_equal(-1, chop(2, [1, 3, 5, 7]))
assert_equal(-1, chop(4, [1, 3, 5, 7]))
assert_equal(-1, chop(6, [1, 3, 5, 7]))
assert_equal(-1, chop(8, [1, 3, 5, 7]))

//: As extensions

// CCC, 9/15/2015. This is my preferred implementation so far.
extension Array where Element: Comparable {
    func chop(searchTerm: Element) -> Int? {
        func helper(slice: ArraySlice<Element>) -> Int? {
            switch slice.count {
            case 0:
                return nil
            case 1:
                if slice[slice.startIndex] == searchTerm {
                    return slice.startIndex
                } else {
                    return nil
                }
            default:
                let middleIndex = (slice.startIndex + slice.endIndex) / 2
                let middleValue = slice[middleIndex]
                if searchTerm < middleValue {
                    return helper(slice[slice.startIndex..<middleIndex])
                } else {
                    return helper(slice[middleIndex..<slice.endIndex])
                }
            }
        }
        return helper(self[0..<self.count])
    }
}

// Again, this compiles, but I can't seem to invoke it properly. Probably not applicable to an array because SubSequence != Self.
extension CollectionType where Generator.Element: Comparable, Index == Int, SubSequence == Self {
    func chop(searchTerm: Generator.Element) -> Int? {
        func helper(slice: Self) -> Int? {
            switch slice.count {
            case 0:
                return nil
            case 1:
                if slice[slice.startIndex] == searchTerm {
                    return slice.startIndex
                } else {
                    return nil
                }
            default:
                let middleIndex = (slice.startIndex + slice.endIndex) / 2
                let middleValue = slice[middleIndex]
                if searchTerm < middleValue {
                    return helper(slice[slice.startIndex..<middleIndex])
                } else {
                    return helper(slice[middleIndex..<slice.endIndex])
                }
            }
        }
        return helper(self[0..<self.count])
    }
}

assert_equal(nil, [].chop(3))
assert_equal(nil, [1].chop(3))
assert_equal(0,  [1].chop(1))

assert_equal(0,  [1, 3, 5].chop(1))
assert_equal(1,  [1, 3, 5].chop(3))
assert_equal(2,  [1, 3, 5].chop(5))
assert_equal(nil, [1, 3, 5].chop(0))
assert_equal(nil, [1, 3, 5].chop(2))
assert_equal(nil, [1, 3, 5].chop(4))
assert_equal(nil, [1, 3, 5].chop(6))

assert_equal(0,  [1, 3, 5, 7].chop(1))
assert_equal(1,  [1, 3, 5, 7].chop(3))
assert_equal(2,  [1, 3, 5, 7].chop(5))
assert_equal(3,  [1, 3, 5, 7].chop(7))
assert_equal(nil, [1, 3, 5, 7].chop(0))
assert_equal(nil, [1, 3, 5, 7].chop(2))
assert_equal(nil, [1, 3, 5, 7].chop(4))
assert_equal(nil, [1, 3, 5, 7].chop(6))
assert_equal(nil, [1, 3, 5, 7].chop(8))

assert_equal(nil, [].chop("cat"))
assert_equal(0, ["cat", "dog"].chop("cat"))
assert_equal(1, ["cat", "dog", "horse"].chop("dog"))
