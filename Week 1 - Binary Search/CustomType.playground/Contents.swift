//: Hacking around with creating a SortedArray type

import Cocoa

struct SortedArray<Element> {
    typealias Position = Double
    
    var backingDictionary: [Position: Element] = [:]
    
    mutating func insert(newElement: Element) {
        let insertionPosition = insertionPositionForElement(newElement)
        backingDictionary[insertionPosition] = newElement
    }
    
    private func insertionPositionForElement(element: Element) -> Position {
        
    }
}